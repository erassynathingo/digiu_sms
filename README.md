# DIGIU SMS API

## Setup

**Backend**: Nodejs + Express

**Database**: MongoDB

**Database Library**: Mongoose

### Development

**Database Server**: **Specify**

### Config Paths

#### App Configs

Application configs and parameters are found in the following file, they are loaded from the environment variables:

```bash
config.js
```

### Environment variables

Create and set the following environmnet variables (`.bash_profile`) from macOS and linux.

```bash
export DIGIU_TW_ACCOUNT_SID=""
export DIGIU_JWT_SECRET=""
export DIGIU_TW_AUTH_TOKEN=""
export DIGIU_TW_SMS_NOT_SID=""
export DIGIU_CREDENTIALS="/path"
export NODE_ENV=""
export DIGIU_INT_CERT_PATH="/path"
export DIGIU_SSL_KEY_PATH="/path"
export DIGIU_SSL_CERT_PATH="/path"

export DIGIU_LOGS_PATH=""
```

Certificates, Logs, and MongoDB config files should be kept out of the working directory, it is recommended that they be 
kept in the home directory instead. The following is the recommended structure

```bash
export DIGIU_CREDENTIALS="/Users/username/credentials/credentials.js"
export DIGIU_INT_CERT_PATH="/Users/username/certs/-.DIGIU.app_ssl_certificate_INTERMEDIATE.cer"
export DIGIU_SSL_KEY_PATH="/Users/username/certs/*.DIGIU.app_private_key.key"
export DIGIU_SSL_CERT_PATH="/Users/username/certs/DIGIU.app_ssl_certificate.cer"
export DIGIU_LOGS_PATH="/Users/username/logs"
```

#### Routes Configs

Routes configuration are found in the following file:

```bash
routes.config.js
```

## Getting Started

### 1. Clone the repo

```bash
git clone https://gitlab.com/erassynathingo/digiu_sms.git
```

### 2. Install NPM Modules

####Using npm
```bash
npm install
```

Install pm2 if not already installed

```bash
npm install -g pm2
```
#### Using yarn
```bash
yarn add
```

Install pm2 if not already installed

```bash
yarn global add pm2
```

### 3. Change configurations

- Specify your database credentials and other parameters.
- Specify any other parameters specific to your project or environment.

### 4. Run the project


for development

```bash
npm start
```

for production

```bash
npm run prod
```
