/**
* @author Erastus Nathingo <contact@erassy.com>
* @module agenda.route.js 
* @description routes SMS requests from the MNO
* @param {Object} dbConfig -  database configurations
* @returns {new Class}
*/

const config = require('../config');
const Log = require('../libraries/logger.lib');
let express = require('express');
let bodyParser = require('body-parser');
let Auth = require('../middleware/auth.middleware');
const Controller = require('../core/controller.factory');
const RBAC = require('../libraries/authorize.lib')
const roles = require('../config/roles.config');
/** Agenda Requirements */
let Agenda = require('agenda');
const Agendash = require('agendash');
const agendaUI = require('agenda-ui');
const agenda = new Agenda({db: {address: config.db_url}});

let logger = new Log();
let auth = new Auth();
let authorizer = new RBAC(roles);
const controller = Controller.create('mno.model');

let router = express.Router();
let app = express();


router.get('/',(req, res, next) => {
    console.log("\n\nDash: ", Agendash(agenda, {title: 'DIGIU Scheduled Messages'}), "\n\n")
    return Agendash(agenda, {title: 'DIGIU Scheduled Messages'})
});


router.get('/ui',(req, res, next) => {
    console.log("\n\nUI: ", agendaUI(agenda, {poll: 1000}), "\n\n")
    return agendaUI(agenda, {poll: 1000})
});

module.exports = router;
