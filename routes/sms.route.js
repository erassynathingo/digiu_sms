/**
* @author Erastus Nathingo <contact@erassy.com>
* @module SMS_Route
* @description routes SMS requests from the client
* @param
* @returns {Object} response object
* @throws {Not Found Error} route not found error
*/

const config = require('../config');
const Log = require('../libraries/logger.lib');
const _ = require('lodash');
let express = require('express');
let bodyParser = require('body-parser');
let errorHandler = require('../libraries/errorHandler.lib');
let Auth = require('../middleware/auth.middleware');
let NotFoundError = require('../libraries/errors/NotFound');
const Controller = require('../core/controller.factory');
const SMPP = require('../libraries/smpp.lib');
const RBAC = require('../libraries/authorize.lib')
const roles = require('../config/roles.config');
const Sorter = require('../helpers/mno.sort');
const helpers = require('../libraries/api.helpers');

const Hash = require('../libraries/hash.lib');
const hash = new Hash();
let logger = new Log();
let auth = new Auth();
let authorizer = new RBAC(roles);
const controller = Controller.create('sms.model');
const inbox = Controller.create('inbox.model');
const Models = require('../core/models.exporter')
const Throttler = require('../core/smpp.throttle');
const models = new Models(Controller);

/** Http Request library */
const $http = require('../libraries/http.request.lib');
const request = require('request-promise');

let router = express.Router();
let app = express();

/**
 * @description SMS configurations
 */
let currentSession = null;


router.post('/', auth.authenticate, (req, res, next) => {
    let sorter = new Sorter();
    authorizer.can(req.currentUser,'CREATE', `CREATING SMS ${JSON.stringify(req.body)}`).then( data=>{
      return helpers.campaignify(req.body, req.currentUser, models.campaign())
    }).then(data => {
      return helpers.own(req.body, req.currentUser)
    }).then(ownedSms => {
      return sorter.sort(ownedSms.recepients).then(msisdns => {
        logger.log("Sorting");
        if(msisdns.MTC.length > 0){
          logger.log("Preparing MTC");
          sorter.prepare('MTC',_.cloneDeep(ownedSms), msisdns.MTC)
          .then(data=>{
            logger.log("Scheduling checker");
            // check Put models into schedule
            return data.scheduling.status == true ? sorter.schedule(models.mtc_p0(), models.mtc_p1() ,models.mtc_p2(), models.mtc_p3(), data) : helpers.duplicate(data);
            
          }).then(duplicated => {
            if(duplicated == undefined){
              logger.log("Message to be Scheduled");
              return Promise.resolve("Message Successfully Scheduled");
            } else{
              logger.log("Scheduling not specified...Proceeding");
              return helpers.prioritize(models.mtc_p0(), models.mtc_p1() ,models.mtc_p2(), models.mtc_p3(), duplicated)
            }
          })
          .then((data) => {
            console.log('\n\nTransaction completed: Resolving: ',data, '\n\n');
            logger.log(`Transaction completed: Resolving: ${JSON.stringify(data)}`);
            res.$json(data);
          })
        }

        if(msisdns.TNMobile.length > 0){
          logger.log("Preparing TN Mobile");
          sorter.prepare('TN_MOBILE',_.cloneDeep(ownedSms), msisdns.TNMobile)
          .then(data=>{
            logger.log("Scheduling checker");
            // check Put models into schedule
            return data.scheduling.status == true ? sorter.schedule(models.tn_p0(), models.tn_p1() ,models.tn_p2(), models.tn_p3(), data) : helpers.duplicate(data);
            
          }).then(duplicated => {
            if(duplicated == undefined){
              logger.log("Message to be Scheduled");
              return Promise.resolve("Message Successfully Scheduled");
            } else{
              logger.log("Scheduling not specified...Proceeding");
              return helpers.prioritize(models.tn_p0(), models.tn_p1() ,models.tn_p2(), models.tn_p3(), duplicated)
            }
          })
          .then((data) => {
            logger.log(`Transaction completed: Resolving: ${JSON.stringify(data)}`);
            res.$json(data);
          })
        }
        /** Numbers not belonging to MTC or TN Mobile forward to ESB */
        if(msisdns.invalidMSISDN.length > 0){
          logger.log("Preparing to send to ESB");
          console.log("Preparing to send to ESB");
          console.log("RAW XML: ", rawSMS);
          let options = {
            method: 'POST',
            url: config.ESB_SMS_URL,
            keepAlive: false,
            headers: {
                'Content-Type': 'application/xml'
            },
            body: rawSMS,
            strictSSL: false,
            // rejectUnauthorized: false,
            proxy: "",
            resolveWithFullResponse: true,
            // The below parameters are specific to request-retry 
            maxAttempts: 3, // (default) try 3 times 
            retryDelay: 2000, // (default) wait for 5s before trying again 
            retryStrategy: request.RetryStrategies.HTTPOrNetworkError // (default) retry on 5xx or network errors 
          }
          $http.send(options).then(response => {
            console.log("ESB RESPONSE: ",response);
            res.$json(response);
          }).catch(error => {
            console.log("ESB RESPONSE ERROR: ", error);
            logger.log(JSON.stringify(error));
          })
        }

      })
    }).catch((error) => {
      res.$error.resolve(res, error, config.response.status_codes.NOT_CREATED);
    });
  })
  router.patch('/:id', auth.authenticate, (req, res, next) => {
    authorizer.can(req.currentUser,'PATCH', `SMS ID ${req.params.id}`).then(data=>{
      return  controller.patch(req)
    }).then((doc) => {
      logger.log('Password change successful');
      res.$json(doc);
    }).catch((error) => {
      logger.log(' password update unsuccessful').error(error.message);
      res.$error.resolve(res, error, config.response.status_codes.NOT_UPDATED);
    });
  })
  router.delete('/:id', auth.authenticate, (req, res, next) => {
    authorizer.can(req.currentUser,'DELETE', `SMS ID ${req.params.id}`).then(data=>{
      return  controller.delete(req.params.id)
    }).then((doc) => {
      logger.log('SMS Deletion successful');
      res.$json(doc);
    }).catch((error) => {
      logger.log('SMS Delete resulted in an error').error(error);
      res.$error.resolve(res, error, config.response.status_codes.NOT_DELETED);
    });
  })
  router.get('/inbox', auth.authenticate,(req, res, next) => {
    authorizer.can(req.currentUser,'READ', 'Get all Ibox SMSs').then(data=>{
      return inbox.getAll(req)
    }).then((doc) => {
      logger.log('SMSs retrieval successful');
      res.$json(doc);
    }).catch((error) => {
      console.error("\n\nGetting services: ", error)
      res.$error.resolve(res, error, config.response.status_codes.NOT_FETCHED)
    });
});

  router.get('/:id', auth.authenticate, (req, res, next) => {
      authorizer.can(req.currentUser,'READ', `SMS ID ${req.params.id}`).then(data=>{
        return  controller.getOne(req.params.id)
      }).then((doc) => {
        logger.log('SMS retrieval successful');
        res.$json(doc);
      }).catch((error) => {
        logger.log('SMS retrieval unsuccessful').error(error.message);
        res.$error.resolve(res, error, config.response.status_codes.NOT_FETCHED);
      });
    })
    router.get('/', auth.authenticate,(req, res, next) => {
      authorizer.can(req.currentUser,'READ', 'Get all SMSs').then(data=>{
        return controller.getAll(req)
      }).then((doc) => {
        logger.log('SMSs retrieval successful');
        res.$json(doc);
      }).catch((error) => {
        res.$error.resolve(res, error, config.response.status_codes.NOT_FETCHED)
      });
    
  })

  router.post('/stats',(req, res, next) => {
      console.log("Fetching stats by aggregation query");
      return controller.getStats(req.body.query).then((doc) => {
      logger.log('SMSs Stats retrieval successful');
      res.$json(doc);
    }).catch((error) => {
      res.$error.resolve(res, error, config.response.status_codes.NOT_FETCHED)
    });
  
})



router.post('/query_status', auth.authenticate, (req, res, next) => {
  console.log("Querying SMS Status for ");
  authorizer.can(req.currentUser,'QUERY', 'Query SMS status').then(data => {
     return controller.getStats(req.body.query)
  }).then((doc) => {
    logger.log('SMSs Stats retrieval successful');
    res.$json(doc);
  }).catch((error) => {
    res.$error.resolve(res, error, config.response.status_codes.NOT_FETCHED)
  });

})
  

module.exports = router;
