/**
* @author Erastus Nathingo <contact@erassy.com>
* @module Users_Route
* @description routes User requests from the client
* @param
* @returns {Object} response object
* @throws {Not Found Error} route not found error
*/

const config = require('../config');
const Log = require('../libraries/logger.lib');
let logger = new Log();
let express = require('express');
let bodyParser = require('body-parser');
let Auth = require('../middleware/auth.middleware');
let auth = new Auth();
// let controller = require('../controllers/user.ctrl');
let NotFoundError = require('../libraries/errors/NotFound');

const RBAC = require('../libraries/authorize.lib')
const roles = require('../config/roles.config');

const Hash = require('../libraries/hash.lib');
const hash = new Hash();
const Controller = require('../core/controller.factory');
const controller = Controller.create('users.model');
const authorizer = new RBAC(roles);

let router = express.Router();
let app = express();

router.post('/:id', auth.authenticate, (req, res, next) => {
  authorizer.can(req.currentUser,'UPDATE', `User id ${req.params.id}`).then(data=>{
    return controller.update(req)
  }).then((doc) => {
    logger.log(req.params.id + 'update successful').log(doc);
    res.$json(doc);
  }).catch((error) => {
    logger.log(' update resulted in an error').error(error.message);
    res.$error.resolve(res, error, config.response.status_codes.NOT_UPDATED);
  });
}).post('/', (req, res, next) => {
  //authorizer.can(req.currentUser,'CREATE', 'Create User').then(data=>{})
  hash.encrypt(req.body.password)
  .then(password => {
    req.body.password = password;
    return controller.create(req);
  }).then(auth => {
    res.$json(auth);
  }).catch((error) => {
    console.error(error);
    res.$error.resolve(res, error, config.response.status_codes.NOT_CREATED);
  });
}).patch('/', auth.authenticate, async (req, res, next) => {

  try {
    let authorized = authorizer.can(req.currentUser,'PATCH', `User id ${req.params.id}`);
    let reset = await controller.resetPassword(req);
    logger.log('Password change successful').log(JSON.stringify(reset));
    res.$json(reset);
  } catch (error) {
    logger.log(' password update unsuccessful').error(error.message);
    res.$error.resolve(res, error, config.response.status_codes.NOT_UPDATED);
  }

}).delete('/:id', auth.authenticate, (req, res, next) => {
  authorizer.can(req.currentUser,'DELETE', `User id ${req.params.id}`).then(data=>{
    return controller.delete(req.params.id)
  }).then((doc) => {
    logger.log('User Deletion successful').log(doc);
    res.$json(doc);
  }).catch((error) => {
    logger.log(' fetch resulted in an error').error(error);
    res.$error.resolve(res, error, config.response.status_codes.NOT_DELETED);
  });
}).get('/:id', auth.authenticate, (req, res, next) => {
  authorizer.can(req.currentUser,'READ', `User id ${req.params.id}`).then(data=>{
    return controller.getOne(req.params.id)
  }).then((doc) => {
    logger.log('User retrieval successful').log(doc);
    res.$json(doc);
  }).catch((error) => {
    logger.log('User retrieval unsuccessful').error(error.message);
    res.$error.resolve(res, error, config.response.status_codes.NOT_FETCHED);
  });
}).get('/', auth.authenticate, (req, res, next) => {
  authorizer.can(req.currentUser,'READ', 'Get all Users').then(data=>{
    return controller.getAll(req)
  }).then((doc) => {
    logger.log('User retrieval successful').log(doc);
    res.$json(doc);
  }).catch((error) => {
    logger.log(' retrieval unsuccessful').error(error.message);
    res.$error.resolve(res, error, config.response.status_codes.NOT_FETCHED);
  });
});

module.exports = router;
