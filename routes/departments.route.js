/**
* @author Erastus Nathingo <contact@erassy.com>
* @module Departments_Route
* @description routes Department based requests from the Department
* @param
* @returns {Object} response object
*/

let express = require('express');
let bodyParser = require('body-parser');
const config = require('../config');
const Log = require('../libraries/logger.lib');
let errorHandler = require('../libraries/errorHandler.lib');
let Auth = require('../middleware/auth.middleware');
const Hash = require('../libraries/hash.lib');
const Controller = require('../core/controller.factory');
const controller = Controller.create('departments.model');
const RBAC = require('../libraries/authorize.lib')
const roles = require('../config/roles.config');

let auth = new Auth();
let logger = new Log();
const hash = new Hash();
let authorizer = new RBAC(roles);

let router = express.Router();
let app = express();

router.post('/:id', auth.authenticate, (req, res, next) => {
  authorizer.can(req.currentUser, 'UPDATE', `Department ID ${req.params.id}`).then(data => {
    return controller.update(req);
  }).then((doc) => {
    logger.log(req.params.id + 'update successful');
    res.$json(doc);
  }).catch((error) => {
    logger.log(' update resulted in an error').error(error.message);
    res.$error.resolve(res, error, config.response.status_codes.NOT_UPDATED);
    res.$json(json);
  });
}).post('/', auth.authenticate, (req, res, next) => {
  authorizer.can(req.currentUser, 'CREATE', `Department ID ${req.body._id}`).then(data => {
    return controller.create(req);
  }).then(auth => {
    res.$json(auth);
  }).catch((error) => {
    res.$json(error);
  });
}).patch('/:id', auth.authenticate, (req, res, next) => {
  authorizer.can(req.currentUser, 'PATCH', `Department ID ${req.params.id}`).then(data => {
    return controller.patch(req)
  }).then((doc) => {
    logger.log('Password change successful');
    res.$json(doc);
  }).catch((error) => {
    logger.log('Password update unsuccessful').error(error.message);
    res.$error.resolve(res, error, config.response.status_codes.NOT_UPDATED);
  });
}).delete('/:id', auth.authenticate, (req, res, next) => {
  authorizer.can(req.currentUser, 'DELETE', `Department ID ${req.params.id}`).then(data => {
    return controller.delete(req.params.id)
  }).then((doc) => {
    logger.log('Department Deletion successful');
    res.$json(doc);
  }).catch((error) => {
    logger.log('Department Delete resulted in an error').error(error);
    res.$error.resolve(res, error, config.response.status_codes.NOT_DELETED);
  });
}).get('/:id', auth.authenticate, (req, res, next) => {
    authorizer.can(req.currentUser, 'READ', `Department ID ${req.params.id}`).then(data => {
      return controller.getOne(req.params.id)
    }).then((doc) => {
      logger.log('Department retrieval successful');
      res.$json(doc);
    }).catch((error) => {
      logger.log('Department retrieval unsuccessful').error(error.message);
      res.$error.resolve(res, error, config.response.status_codes.NOT_FETCHED);
    });
  }).get('/', auth.authenticate, (req, res, next) => {
    authorizer.can(req.currentUser, 'READ', `Department ID ${req.params.id}`).then(data => {
      return controller.getAll(req)
    }).then((doc) => {
    logger.log('Departments retrieval successful');
    res.$json(doc);
  }).catch((error) => {
    logger.log('Departments retrieval unsuccessful').error(error.message);
    res.$error.resolve(res, error, config.response.status_codes.NOT_FETCHED);
  });
});

module.exports = router;
