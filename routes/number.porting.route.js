/**
* @author Erastus Nathingo <contact@erassy.com>
* @module Number_Portability_Route
* @description routes number portability requests
* @param
* @returns {Object} response object
*/

let express = require('express');
let bodyParser = require('body-parser');
const config = require('../config');
const Log = require('../libraries/logger.lib');
let errorHandler = require('../libraries/errorHandler.lib');
let Auth = require('../middleware/auth.middleware');
const Hash = require('../libraries/hash.lib');

const Controller = require('../core/controller.factory');
const Models = require('../core/models.exporter');
const mtc_controller = new Models(Controller).mtc_port_list();
const tn_controller = new Models(Controller).tn_port_list();


const RBAC = require('../libraries/authorize.lib')
const roles = require('../config/roles.config');

let auth = new Auth();
let logger = new Log();
const hash = new Hash();
let authorizer = new RBAC(roles);

let router = express.Router();
let app = express();


/** MTC Number portability routes */

router.patch('/mtc/:id', auth.authenticate, (req, res, next) => {
  authorizer.can(req.currentUser, 'UPDATE', `MNO port list ID ${req.params.id}`).then(data => {
    return mtc_controller.update(req);
  }).then((doc) => {
    logger.log(req.params.id + 'update successful');
    res.$json(doc);
  }).catch((error) => {
    logger.log(' update resulted in an error').error(error.message);
    res.$error.resolve(res, error, config.response.status_codes.NOT_UPDATED);
    res.$json(json);
  });
}).post('/mtc', auth.authenticate, (req, res, next) => {
  authorizer.can(req.currentUser, 'CREATE', `MNO port list ID ${req.body._id}`).then(data => {
    return mtc_controller.create(req);
  }).then(auth => {
    res.$json(auth);
  }).catch((error) => {
    res.$json(error);
  });
}).delete('/mtc/:id', auth.authenticate, (req, res, next) => {
  authorizer.can(req.currentUser, 'DELETE', `MNO port list ID ${req.params.id}`).then(data => {
    return mtc_controller.delete(req.params.id)
  }).then((doc) => {
    logger.log('MNO port list Deletion successful');
    res.$json(doc);
  }).catch((error) => {
    logger.log('MNO port list Delete resulted in an error').error(error);
    res.$error.resolve(res, error, config.response.status_codes.NOT_DELETED);
  });
}).get('/mtc', auth.authenticate, (req, res, next) => {
    authorizer.can(req.currentUser, 'READ', `MNO port list ID ${req.params.id}`).then(data => {
      return mtc_controller.getAll(req)
    }).then((doc) => {
    logger.log('MNO port lists retrieval successful');
    res.$json(doc);
  }).catch((error) => {
    logger.log('MNO port lists retrieval unsuccessful').error(error.message);
    res.$error.resolve(res, error, config.response.status_codes.NOT_FETCHED);
  });
})

/** TN Mobile Number portability routes */

.patch('/tn/:id', auth.authenticate, (req, res, next) => {
    authorizer.can(req.currentUser, 'UPDATE', `MNO port list ID ${req.params.id}`).then(data => {
      return tn_controller.update(req);
    }).then((doc) => {
      logger.log(req.params.id + 'update successful');
      res.$json(doc);
    }).catch((error) => {
      logger.log(' update resulted in an error').error(error.message);
      res.$error.resolve(res, error, config.response.status_codes.NOT_UPDATED);
      res.$json(json);
    });
  }).post('/tn', auth.authenticate, (req, res, next) => {
    authorizer.can(req.currentUser, 'CREATE', `MNO port list ID ${req.body._id}`).then(data => {
      return tn_controller.create(req);
    }).then(auth => {
      res.$json(auth);
    }).catch((error) => {
      res.$json(error);
    });
  }).delete('/tn/:id', auth.authenticate, (req, res, next) => {
    authorizer.can(req.currentUser, 'DELETE', `MNO port list ID ${req.params.id}`).then(data => {
      return tn_controller.delete(req.params.id)
    }).then((doc) => {
      logger.log('MNO port list Deletion successful');
      res.$json(doc);
    }).catch((error) => {
      logger.log('MNO port list Delete resulted in an error').error(error);
      res.$error.resolve(res, error, config.response.status_codes.NOT_DELETED);
    });
  }).get('/tn', auth.authenticate, (req, res, next) => {
      authorizer.can(req.currentUser, 'READ', `MNO port list ID ${req.params.id}`).then(data => {
        return tn_controller.getAll(req)
      }).then((doc) => {
      logger.log('MNO port lists retrieval successful');
      res.$json(doc);
    }).catch((error) => {
      logger.log('MNO port lists retrieval unsuccessful').error(error.message);
      res.$error.resolve(res, error, config.response.status_codes.NOT_FETCHED);
    });
  });

module.exports = router;
