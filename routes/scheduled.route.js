/**
* @author Erastus Nathingo <contact@erassy.com>
* @module Scheduled_Messages_Route
* @description routing for scheduled messages
* @param
* @throws {Not Found Error} route not found error
*/

const config = require('../config');
const Log = require('../libraries/logger.lib');
let express = require('express');
let bodyParser = require('body-parser');
let Auth = require('../middleware/auth.middleware');
let NotFoundError = require('../libraries/errors/NotFound');
const Controller = require('../core/controller.factory');
const RBAC = require('../libraries/authorize.lib')
const roles = require('../config/roles.config');
const Model = require('../core/models.exporter');

const Hash = require('../libraries/hash.lib');
const hash = new Hash();
let logger = new Log();
let auth = new Auth();
let authorizer = new RBAC(roles);
const controller = new Model(Controller).scheduled();

let router = express.Router();
let app = express();


router.post('/:id', auth.authenticate, (req, res, next) => {
  authorizer.can(req.currentUser,'UPDATE', `Scheduled Messages ID ${req.params.id}`).then(data=>{
    return  controller.update(req)
  }).then((doc) => {
      logger.log(req.params.id + 'update successful');
      res.$json(doc);
    }).catch((error) => {
      logger.log('update resulted in an error').error(error.message);
      res.$error.resolve(res, error, config.response.status_codes.NOT_UPDATED);
    });
  }).post('/', auth.authenticate, (req, res, next) => {
    authorizer.can(req.currentUser,'CREATE', `CREATING Scheduled Messages ${req.body}`).then(data=>{
      return  controller.create(req)
    }).then(data => {
      res.$json(data);
    }).catch((error) => {
      res.$error.resolve(res, error, config.response.status_codes.NOT_CREATED);
    });
  }).patch('/:id', auth.authenticate, (req, res, next) => {
    authorizer.can(req.currentUser,'PATCH', `Scheduled Messages ID ${req.params.id}`).then(data=>{
      return  controller.patch(req)
    }).then((doc) => {
      logger.log('Password change successful');
      res.$json(doc);
    }).catch((error) => {
      logger.log(' password update unsuccessful').error(error.message);
      res.$error.resolve(res, error, config.response.status_codes.NOT_UPDATED);
    });
  }).delete('/:id', auth.authenticate, (req, res, next) => {
    authorizer.can(req.currentUser,'DELETE', `Scheduled Messages ID ${req.params.id}`).then(data=>{
      return  controller.delete(req.params.id)
    }).then((doc) => {
      console.log('Scheduled Messages Deletion successful'.green);
      console.log('doc',doc)
      logger.log('Scheduled Messages Deletion successful');
      res.$json(doc);
    }).catch((error) => {
      console.log('Scheduled Messages Deletion unsuccessful'.red);
      logger.log('Scheduled Messages Delete resulted in an error').error(error);
      res.$error.resolve(res, error, config.response.status_codes.NOT_DELETED);
    });
  })
    .get('/:id', auth.authenticate, (req, res, next) => {
      authorizer.can(req.currentUser,'READ', `Scheduled Messages ID ${req.params.id}`).then(data=>{
        return   controller.getOne(req)
      }).then((doc) => {
        logger.log('Scheduled Messages retrieval successful');
        res.$json(doc);
      }).catch((error) => {
        logger.log('Scheduled Messages retrieval unsuccessful').error(error.message);
        res.$error.resolve(res, error, config.response.status_codes.NOT_FETCHED);
      });
    }).get('/', auth.authenticate,(req, res, next) => {
      authorizer.can(req.currentUser,'READ', 'Get all Scheduled Messagess').then(data => {
        return controller.getAll(req)
      }).then((doc) => {
        logger.log('Scheduled Messagess retrieval successful');
        res.$json(doc);
      }).catch((error) => {
        res.$error.resolve(res, error, config.response.status_codes.NOT_FETCHED)
      });
    
  });

module.exports = router;
