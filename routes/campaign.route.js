

const config = require('../config');
const Log = require('../libraries/logger.lib');
let logger = new Log();
let express = require('express');
let bodyParser = require('body-parser');
let Auth = require('../middleware/auth.middleware');
let auth = new Auth();
// let controller = require('../controllers/user.ctrl')
let NotFoundError = require('../libraries/errors/NotFound');

const RBAC = require('../libraries/authorize.lib')
const roles = require('../config/roles.config');

const Hash = require('../libraries/hash.lib');
const hash = new Hash();
const Controller = require('../core/controller.factory');
const controller = Controller.create('campaign.model');
const authorizer = new RBAC(roles);

let router = express.Router();
let app = express();

router.post('/:id', auth.authenticate, (req, res, next) => {
  authorizer.can(req.currentUser, 'UPDATE', `Campaign ID ${req.params.id}`).then(data => {
    return controller.update(req);
  }).then((doc) => {
    logger.log(req.params.id + 'update successful');
    res.$json(doc);
  }).catch((error) => {
    logger.log(' update resulted in an error').error(error.message);
    res.$error.resolve(res, error, config.response.status_codes.NOT_UPDATED);
    res.$json(json);
  });
}).post('/', auth.authenticate, (req, res, next) => {
  authorizer.can(req.currentUser, 'CREATE', `Campaign ID ${req.body._id}`).then(data => {
    return controller.create(req);
  }).then(auth => {
    res.$json(auth);
  }).catch((error) => {
    res.$json(error);
  });
}).patch('/:id', auth.authenticate, (req, res, next) => {
  authorizer.can(req.currentUser,'PATCH', `Campaign id ${req.params.id}`).then(data=>{
    return controller.patch(req)
  }).then((doc) => {
    logger.log('Campaign Update successful').log(doc);
    res.$json(doc);
  }).catch((error) => {
    logger.log(' Campaign update unsuccessful').error(error.message);
    res.$error.resolve(res, error, config.response.status_codes.NOT_UPDATED);
  });
}).get('/:id', auth.authenticate, (req, res, next) => {
  authorizer.can(req.currentUser,'READ', `Campaign id ${req.params.id}`).then(data=>{
    return controller.getOne(req.params.id)
  }).then((doc) => {
    logger.log('Campaign retrieval successful').log(doc);
    res.$json(doc);
  }).catch((error) => {
    logger.log('Campaign retrieval unsuccessful').error(error.message);
    res.$error.resolve(res, error, config.response.status_codes.NOT_FETCHED);
  });
}).get('/', auth.authenticate, (req, res, next) => {
  authorizer.can(req.currentUser,'READ', 'Get all Users').then(data=>{
    return controller.getAll(req)
  }).then((doc) => {
    logger.log('Campaign retrieval successful').log(doc);
    res.$json(doc);
  }).catch((error) => {
    logger.log(' Campaign unsuccessful').error(error.message);
    res.$error.resolve(res, error, config.response.status_codes.NOT_FETCHED);
  });
});

module.exports = router;
