/**
* @author Erastus Nathingo <contact@erassy.com>
* @module Authentication Route
* @description route to handle auth requests
* @param
* @returns {Object} response object
* @throws {Not Found Error} route not found error
*/

const config = require('../config');
const Log = require('../libraries/logger.lib');
let express = require('express');
let errorHandler = require('../libraries/errorHandler.lib');
let Auth = require('../middleware/auth.middleware');
let Response = require('../middleware/responder.middleware');

let auth = new Auth();
let logger = new Log();

let app = express();

let router = express.Router();

router.post('/', (req, res) => {
  auth.login(req).then((data) => {
    if (data) {
      res.setHeader('auth_token', data);
      res.setHeader('access-control-expose-headers', 'auth_token');
      res.$json(req.user);
    }
  }).catch((error) => {
    console.log('Err: ', error.message);
    logger.log('login threw an error: =>').log(error.message);
    res.$error.resolve(res, error, 401);
  });
}).get('/', (req, res) => {
  auth.getAuthenticated(req).then((user) => {
    res.$json(user);
  }).catch((error) => {
    logger.log('Getting User').log(error.message);
    res.$error.resolve(res, error, config.response.status_codes.UNAUTHORIZED);
  });
}).patch('/', (req, res) => {
  auth.update(req).then((user) => {
    logger.log('update successful.').log(user);
    res.$json(user);
  }).catch((error) => {
    logger.log('Patch threw an error: =>').log(error.message);
    res.$error.resolve(res, error, config.response.status_codes.NOT_FOUND);
  });
}).delete('/', (req, res) => {
  auth.logout(req).then((data) => {
    logger.log('logged out successful').log(data);
    let json = { status: config.response.status_codes.DELETED, message: 'User log out successful' };
    res.$json(json);
  }).catch((error) => {
    logger.log('logout threw an error: =>').log(error.message);
    res.$error.resolve(res, error, config.response.status_codes.SERVER_ERROR);
  });
});

module.exports = router;
