/**
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @description Library for creating root controllers
 * @copyright (c) 2018 DIGIU
 */


const Controller = require('./base.controller');
const Log = require('../libraries/logger.lib');
let logger = new Log();
const _ = require('lodash');

let ControllerFactory = function () {
    return {
        create: function (model) {
            let Model = require("../models/" + model);
            return new Controller(Model);
        }
    }
}
module.exports = ControllerFactory();