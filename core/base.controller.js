/**
 * Base Controller Functionality
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @description Base Model => Returns model functions
 * @copyright (c) 2018 DIGIU
 * @license MIT
 */
const _ = require('lodash');
let Promise = require('bluebird');
const Log = require('../libraries/logger.lib');
const Logger = new Log();
const helpers = require('../libraries/api.helpers');
const QueryBuilder = require('../libraries/api.query-builder');
const Encryptor = require('../libraries/hash.lib');
const encryptor =  new Encryptor();


let prototype = {
  getAll: function (req) {
    return this.queryBuilder.buildQuery(req).then(pipeline => {
      console.log('Pipeline: ', pipeline); /** @todo Remove */
      return this.model.aggregate(pipeline).then(this.translate);
    });
  },
  get: function (req) {
    return this.model.aggregate(req.query);
  },

  getStats: function (array) {
    return this.model.aggregate(array);
  },

  create: function (req) {
    return this.reverse(req.body, this.createExclude).then(data => {
      return this.model.create(data).then(this.translate);
    });
  },
  createSMS: function (sms) {
    return this.reverse(sms, this.createExclude).then(data => {
      return this.model.create(data).then(this.translate);
    });
  },
  delete: function (id) {
    return this.model.findOneAndRemove({ _id: id }).then(this.translate);
  },

  deleteMany: function (req) {
    return this.model.remove({ userUID: uid, _id: { $in: req}});
  },
  update: async function (req) {
    const reversed = await this.reverse(req.body, this.updateExclude);
    const findAndUpdate = await this.model.findOneAndUpdate({ _id: req.params.id }, reversed, {new: true}).then(this.translate);
    return findAndUpdate;
  },
  updateSMS: async function (_id, data, type) {
    let body = null;
    if (type === 'BULK') {
      return this.model.findOneAndUpdate({ _id: _id }, {$push: { BULK: data }}, {new: true, upsert: true});
    }else if (type === 'OTP') {
      data.body = await encryptor.encryption(data.body);
      return this.model.findOneAndUpdate({ _id: _id }, {$push: { OTPS: data }}, {new: true, upsert: true});
    }else if (type === 'ALERTS') {
      /** Encrypting the ALERT Body */
      data.body = await encryptor.encryption(data.body);
      return this.model.findOneAndUpdate({ _id: _id }, {$push: { ALERTS: data }}, {new: true, upsert: true});
    }
  },
  updateCampaign: function (_id, data) {
    return this.model.findOneAndUpdate({ _id: _id }, {$push: { replies: data }}, {new: true, upsert: true})
  },


  updateQueryStatus: function (_id, data) {
    if (data.type === 'OTP') {
      return this.model.update({_id: data.recepient, 'OTPS.message_id': _id }, {$set: { 'OTPS.$.sms_status': data.sms_status }}, {new: true, upsert: true})

    }else if (data.type === 'ALERTS') {
      return this.model.update({_id: data.recepient, 'ALERTS.message_id': _id }, {$set: { 'ALERTS.$.sms_status': data.sms_status }}, {new: true, upsert: true});
    }
  },

  updatePartial: function (req) {
    return this.reverse(req.body, this.updateExclude).then(data => {
      return this.model.findOneAndUpdate({ _id: req._id }, data, {new: true}).then(this.translate);
    });
  },

  patch: async function (req) {
    return  await this.model.findOneAndUpdate({ _id: req.params.id }, {$set: {password: req.body.password}}, {new: true})
  },
  getOne: function (id) {
    return this.model.findOne({ _id: id }).then(this.translate);
  },
  getActiveCampaign: function () {
    return this.model.find({ status: 'ACTIVE' }, '_id campaignId', function (err, docs) {
    });
  },

  fetch: function (options) {
    return this.model.find();
  },

  findInArray: function(options){
    return this.model.find(options)
  },

  deleteAll: function () {
    return this.model.remove({});
  },

  /** Campaign Unique functions */

  fetchActive: function (id) {
    return this.model.find({ campaignId: id }).where('status').equals('ACTIVE');
  },

  updateLoginCount: function (user, count) {
    return this.model.findOneAndUpdate();
  },
  
  resetPassword: async function(req) {
    const hashed = await encryptor.encrypt(req.body.password);
    return  updatePassword = await this.model.findOneAndUpdate({ _id: req.body.id}, {password: hashed});
  },

  rollback: function (id, data = null , insert = false) {
    console.log('Rolling back...'); /** @todo Remove */
    if (data && insert === true) {
      return this.reverse(data, this.createExclude).then(data => {
        return this.model.create(data);
      });
    } else if (data) {
      return this.reverse(data, this.createExclude).then(data => {
        return this.model.findOneAndUpdate({ _id: id }, data);
      });
    } else {
      return this.model.findOneAndRemove({ _id: id });
    }
  },
  rollbackSms: function (id, data = null , insert = false) {
    console.log('Rolling back...'); /** @todo Remove */
    if (data && insert === true) {
      return this.reverse(data, this.createExclude).then(data => {
        return this.model.create(data);
      });
    } else if (data) {
      return this.reverse(data, this.createExclude).then(data => {
        return this.model.findOneAndUpdate({ _id: id }, data);
      });
    } else {
      return this.model.findOneAndRemove({ message_id: id });
    }
  }
/** @todo search api */
};
function Controller (model) {
  this.model = model.model;
  this.dictionary = model.dictionary;
  this.updateExclude = model.updateExclude;
  this.createExclude = model.createExclude;
  this.readExclude = model.readExclude;
  this.queryBuilder = QueryBuilder.create(model.Schema, model.dictionary);
  /**
   * Wrapper to translate data from database keys to api keys.
   * @param {object} - The data coming from the database to be translated.
   * @return {Promise}
   */
  this.translate = (data) => {
    return helpers.mapInverse(data, this.dictionary);
  };

  /**
   * Wrapper to reverse data keys from api keys to database keys.
   * @param {object} data - The data coming from the api.
   * @param {array} exclude - The keys to be excluded from translation and from propagating to database.
   * @return {Promise}
   */
  this.reverse = (data, exclude = []) => {
    return helpers.map(_.omit(data, exclude), this.dictionary);
  };
}
Controller.prototype = prototype;
module.exports = Controller;
