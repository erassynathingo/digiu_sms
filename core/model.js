/**
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @description Base Model => Returns model functions
 * @copyright (c) 2018 DIGIU
 */
require('mongoose-geojson-schema');
const db = require('../libraries/db');
const ai = require('mongoose-auto-increment');

class Model {

  constructor (name, schema, dictionary, createExclude, updateExclude, readExclude) {
    this.name = name;
    this.schema = schema;
    this.dictionary = dictionary;
    this.createExclude = createExclude;
    this.updateExclude = updateExclude;
    this.readExclude = readExclude;
    this.exclusive = [];
    this.model = new db.Schema(this.schema);
    this.authMap = null;
  }
  enableAI (field = '_id' , start = 1) {
    ai.initialize(db.connection);
    this.model.plugin(ai.plugin, { model: this.name, field: field, startAt: start }); /** Add Auto increment plugin */
    return this;
  }
  getModel () {
    return {
      model: db.model(this.name, this.model),
      dictionary: this.dictionary,
      createExclude: this.createExclude,
      updateExclude: this.updateExclude,
      readExclude: this.readExclude,
      exclusive: this.exclusive,
      authMap: this.authMap,
      schema: this.schema
    };
  }
  createIndex (index, options) {
    this.model.index(index, options);
    return this;
  }
  /**
  * Adds a virtual property, getter and setter for the virtual property.
  * @param {string} name - The name of the virtual property
  * @param {function} getter - The getter function for the property.
  * @param {function} setter - The setter function for the property.
  * @returns {Model} <code>this</code>
  */
  createVirtual (name, getter, setter) {
    if (getter && typeof getter === 'function') {
      this.model.virtual(name).get(getter);
    }
    if (setter && typeof setter === 'function') {
      this.model.virtual(name).set(setter);
    }
    return this;
  }

  addAuthMap (map) {
    this.authMap = map;
    return this;
  }

  addExclusive (exclusive) {
    this.exclusive = exclusive;
    return this;
  }
}

module.exports = {
  create: function (name, schema, dictionary, createExclude = [] , updateExclude = [] , readExclude = []) {
    return new Model(name, schema, dictionary, createExclude, updateExclude, readExclude);
  },
  model: Model,
  Schema: db.Schema
};
