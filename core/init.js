/**
 * init.module.ts
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018 DIGIU
 * All rights reserved
 */


const Throttler = require('../core/smpp.throttle');
const Log = require('../libraries/logger.lib');
const Controller = require('./controller.factory');
let logger = new Log();
const Models = require('./models.exporter');
const models = new Models(Controller);
const SMPP = require('../libraries/smpp.lib');
const _config = require('../config');
const colors = require('colors');




let MTC_ALPHA = {
    start: async () => {
        logger.log(`Checking MTC_ALPHA Sessions`);
        try {
        const getMNO = await models.mno().getOne('MTC_NA_01');
        global.MTC_NA_01 = getMNO;
        const createSession = await new SMPP(getMNO).createSession();
        new Throttler(createSession.session, getMNO, {
            P0: models.mtc_p0(),
            P1: models.mtc_p1(),
            P2: models.mtc_p2(),
            P3: models.mtc_p3(),
            SMS: models.sms(),
            primary_enroute: models.mtc_enroute_primary(),
            //secondary_enroute: models.mtc_enroute_secondary(),
            All: models
          }).init();
        }catch(error){
            console.error(`SMPP Error\nCode: ${error.status_code}\nMessage: ${JSON.stringify(error.message)}\n`.red);
            logger.error(`SMPP Error\nCode: ${error.status_code}\nMessage: ${JSON.stringify(error.message)}\n`.red);
        }
    }
}
let MTC_NUMERIC = {
    start: async () => {
        logger.log(`Checking MTC_NUMERIC Sessions`);
        try {
        const getMNO = await models.mno().getOne('MTC_NA_02');
        global.MTC_NA_02 = getMNO; 
        const createSession = await new SMPP(getMNO).createSession();
        new Throttler(createSession.session, getMNO, {
            P0: models.mtc_p0(),
            P1: models.mtc_p1(),
            P2: models.mtc_p2(),
            P3: models.mtc_p3(),
            SMS: models.sms(),
            primary_enroute: models.mtc_enroute_primary(),
            //secondary_enroute: models.mtc_enroute_secondary(),
            All: models
          }).init();
        }catch(error){
            console.error(`SMPP Error\nCode: ${error.status_code}\nMessage: ${JSON.stringify(error.message)}\n`.red);
            logger.error(`SMPP Error\nCode: ${error.status_code}\nMessage: ${JSON.stringify(error.message)}\n`.red);
        }
    }
}
let TN_MOBILE_ALPHA = {
    start: async () => {
        logger.log(`Checking TN_MOBILE_ALPHA Sessions`);
        try {
        const getMNO = await models.mno().getOne('TN_MOBILE_NA_01');
        global.TN_MOBILE_NA_01 = getMNO; 
        const createSession = await new SMPP(getMNO).createSession();
        new Throttler(createSession.session, getMNO, {
            P0: models.tn_p0(),
            P1: models.tn_p1(),
            P2: models.tn_p2(),
            P3: models.tn_p3(),
            SMS: models.sms(),
            primary_enroute: models.tn_enroute_primary(),
            //secondary_enroute: models.tn_enroute_secondary(),
            All: models
          }).init();
        }catch(error){
            console.error(`SMPP Error\nCode: ${error.status_code}\nMessage: ${JSON.stringify(error.message)}\n`.red);
            logger.error(`SMPP Error\nCode: ${error.status_code}\nMessage: ${JSON.stringify(error.message)}\n`.red);
        }
    }
}

let TN_MOBILE_NUMERIC = {
    start: async () => {
        logger.log(`Checking TN_MOBILE_NUMBRIC Sessions`);
        try {
        const getMNO = await models.mno().getOne('TN_MOBILE_NA_02');
        global.TN_MOBILE_NA_02 = getMNO; 
        const createSession = await new SMPP(getMNO).createSession();
        new Throttler(createSession.session, getMNO, {
            P0: models.tn_p0(),
            P1: models.tn_p1(),
            P2: models.tn_p2(),
            P3: models.tn_p3(),
            SMS: models.sms(),
            primary_enroute: models.tn_enroute_primary(),
            //secondary_enroute: models.tn_enroute_secondary(),
            All: models
          }).init();
        }catch(error){
            console.error(`SMPP Error\nCode: ${error.status_code}\nMessage: ${JSON.stringify(error.message)}\n`.red);
            logger.error(`SMPP Error\nCode: ${error.status_code}\nMessage: ${JSON.stringify(error.message)}\n`.red);
        }
    }
}
let TEST_SERVER = {
    start: async () => {
        logger.log(`Checking TEST_SERVER Sessions`);
        try {

        const getMNO = await models.mno().getOne('TEST_SERVER_01');
        global.TEST_SERVER_01 = getMNO; 
        const createSession = await new SMPP(getMNO).createSession();
        new Throttler(createSession.session, getMNO, {
            P0: models.mtc_p0(),
            P1: models.mtc_p1(),
            P2: models.mtc_p2(),
            P3: models.tn_p3(),
            SMS: models.sms(),
            primary_enroute: models.tn_enroute_primary(),
            //secondary_enroute: models.mtc_enroute_secondary(),
            All: models
          }).init();
        }catch(error){
            console.error(`SMPP Error\nCode: ${error.status_code}\nMessage: ${JSON.stringify(error.message)}\n`.red);
            logger.error(`SMPP Error\nCode: ${error.status_code}\nMessage: ${JSON.stringify(error.message)}\n`.red);
        }
    }
}
let prodStarter = {
    /**
     * @description Creates sessions and starts the throttlers for the MNO's
     * @name startThrottler
     */
      startThrottler : function() {
        logger.log(`Checking Sessions`);
        console.log('Initiating DIGIU SMS GATEWAY..\n'.blue);
          if(_config.SMSC_BIND_CONFIGS.MTC_ALPHA === true){
            console.log('\nAdding SMPP functionality for MTC ALPHANUMERIC...'.blue)
              MTC_ALPHA.start();
          }if(_config.SMSC_BIND_CONFIGS.MTC_NUMERIC === true){
              console.log('\nAdding SMPP functionality for MTC NUMERIC...'.blue)
              MTC_NUMERIC.start();
          }if(_config.SMSC_BIND_CONFIGS.TN_MOBILE_ALPHA === true){
            console.log('\nAdding SMPP functionality for TN_MOBILE ALPHANUMERIC...'.blue)
              TN_MOBILE_ALPHA.start();
          }if(_config.SMSC_BIND_CONFIGS.TN_MOBILE_NUMERIC === true){
            console.log('\nAdding SMPP functionality for TN_MOBILE NUMERIC...'.blue)
              TN_MOBILE_NUMERIC.start();
          }if(_config.SMSC_BIND_CONFIGS.TEST_SERVER === true){
            console.log('\nAdding SMPP functionality for SIMULATOR SMSC...'.blue)
              TEST_SERVER.start();
          }
      }
  }


  //module.exports = devStarter;
  module.exports = prodStarter;