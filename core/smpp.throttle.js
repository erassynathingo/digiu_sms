/**
 * SMPP throttle rate Base Controller
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @description Returns throttle functions
 * @copyright (c) 2018 DIGIU
 * @license MIT
 */
const _ = require('lodash');
let Promise = require('bluebird');
const Log = require('../libraries/logger.lib');
const logger = new Log();
const helpers = require('../libraries/api.helpers');
const QueryBuilder = require('../libraries/api.query-builder');
const Interval = require('Interval');
let __ = require('underscore');
const smpp = Promise.promisifyAll(require('smpp'));
let MessageStateCTRL = require('../controllers/message.state.ctrl');
const moment = require('moment');
const colors = require('colors');
class Throttler {

  /**
   * Represents a Throttler Instance.
   * @constructor
   * @param {Object} session - session of the current MNO for binding purposes
   * @param {Object} mno_configs - Configurations file for the MNO consuming the Throttler
   * @param {Object} Models - Mongoose Models of the Priority collection {Mongo DB Models}
   */

  constructor (session, mno_configs, Models) {
    this.session = session;
    this.mno_configs = mno_configs;
    this.P0 = Models.P0;
    this.P1 = Models.P1;
    this.P2 = Models.P2;
    this.P3 = Models.P3;
    this.SMS = Models.SMS;
    this.Chucked_Messages = [];
    this.Availability = true;
    this.throttle_rate = mno_configs.throttle_rate;
    this.smsStateCtrl = new MessageStateCTRL(Models, this.session, this.mno_configs);
  }

  init () {
    logger.log(`initiating Throttler functions for ${this.mno_configs.full_Name}`);
    console.log(`initiating Throttler functions for ${this.mno_configs.full_Name}`);
    this.smsStateCtrl.init();
    Interval.run(() => {
      if (this.getAvailability() == true) {
        this.chunck();
      }
    }, 1000);
  }

  getThrottler () {
    return {
      session: this.session,
      mno_configs: this.mno_configs
    };
  }

  /**
   * @description Populates the Processed array with messages from Priority collections with respect to priority
   * @name chuck
   * @function
   * @returns {Promise} Chucked_Messages - Array of messages limited by MNO Throttle rate
   */

  chunck () {
    this.resetChucked_Messages();
    // console.log('Availability: ' , this.getAvailability())
    if (this.getAvailability() === true) {
      this.setAvailability(false);
      this.getPriority_Messages(this.P3).then(() => {
        if (this.getChucked_Messages().length < this.throttle_rate) {
          return this.getPriority_Messages(this.P2);
        }else {
          return this.getChucked_Messages();
        }
      }).then(() => {
        if (this.getChucked_Messages().length < this.throttle_rate) {
          return this.getPriority_Messages(this.P1);
        }else {
          return this.getChucked_Messages();
        }
      }).then(() => {
        if (this.getChucked_Messages().length < this.throttle_rate) {
          return this.getPriority_Messages(this.P0);
        }else {
          return this.getChucked_Messages();
        }
      }).then((data) => {
        if (this.getChucked_Messages().length > 0) {
          _.forEach(this.getChucked_Messages(), (message) => {
            this.sendSMS(message);
          });
        }
        this.setAvailability(true);
      }).catch(error => {
        console.log('THrottle Error: ', error);
        this.setAvailability(true);
        logger.error(`Throttler Error: ${JSON.stringify(error)}`).log(`Throttler Error: ${JSON.stringify(error)}`);
      // console.log('Throttler Error:' , error)
      });
    }
  }

  /**
   * @description Handles Sending Single messages to the respective MNO
   * @name sendSMS
   * @param {Object} message 
   * @returns erassy
   * @memberof Throttler
   */
  sendSMS (message) {
    this.setAvailability(false);
    this.session.submit_sm({
      source_addr: this.mno_configs.config.source_addr,
      destination_addr: message.recepient,
      short_message: message.body,
      source_addr_ton: this.mno_configs.config.source_addr_ton,
      dest_addr_ton: this.mno_configs.config.dest_addr_ton,
      esme_addr_ton: this.mno_configs.config.esme_addr_ton,
      addr_ton: this.mno_configs.config.addr_ton,
      addr_npi: this.mno_configs.config.addr_npi,
      esme_addr_npi: this.mno_configs.config.esme_addr_npi,
      system_type: this.mno_configs.config.system_type,
      source_addr_npi: this.mno_configs.config.source_addr_npi,
      esme_addr_npi: this.mno_configs.config.esme_addr_npi,
      dest_addr_npi: this.mno_configs.config.dest_addr_npi,
      /** Always deliver a response */
      registered_delivery: 1
    }, (pdu) => {
      /** Adding response and leaving old date from priority to the message new object */
      let single_1 = __.extend(__.omit(message, ['_id', 'entry_date']), {response: pdu}, {message_id: pdu.message_id});

      /** Adding now Date to the message object */
      let single = __.extend(single_1, {entry_date: new Date()});

      this.SMS.updateSMS(message.recepient, single, message.type).catch(error => {
        console.log('\n\nERROR: ', error);
        logger.log(`SMS Creation Error: ${JSON.stringify(error)}`).error(`SMS Creation Error: ${JSON.stringify(error)}`);
      });
      if (message.mno === 'TN_MOBILE') {
        console.log("TN_Mobile".dim)
        this.smsStateCtrl.setEnrouteMessages(single).catch(error => {
          console.log('\n\nERROR: ', error);
          logger.log(`SMS ENROUTE Creation Error: ${JSON.stringify(error)}`).error(`SMS ENROUTE Creation Error: ${JSON.stringify(error)}`);
        });
      }
    });
  }

  /**
   * @description Sets the availability of the sender function
   * @name setAvailability
   * @function
   * @param {Boolean} status - Availability of the Send function
   * @returns undefined
   */

  setAvailability (status) {
    this.Availability = status;
  }

  /**
   * @description Gets the availability of the sender function
   * @name getAvailability
   * @returns {Boolean}  Availability of the Send function
   */

  getAvailability () {
    return this.Availability;
  }

  /**
  * @description Add Message Objects to the Chucked_Messages Array
  * @name setChucked_Messages
  * @param {Object} message - Availability of the Send function
  * @returns undefined
  */

  setChucked_Messages (message) {
    this.Chucked_Messages.push(message);
  }

  /**
   * @description Gets the Chucked_Messages Array
   * @name getChucked_Messages
   * @returns {Array}  Availability of the Send function
   */

  getChucked_Messages () {
    return this.Chucked_Messages;
  }

  /**
   * @description Gets the Messages from Given Priority  Collection
   * @name resetChucked_Messages
   * @param {Object} Priority_Model - Controller form the given Model
   * @returns {Array}  Array of Messages from respective Priority Collection
   */
  resetChucked_Messages () {
    this.Chucked_Messages = [];
  }
  /**
   * @description Gets the Messages from Given Priority  Collection
   * @name getPriority_Messages
   * {@link https://docs.mongodb.com/manual/reference/operator/aggregation/limit/}
   * @param {Object} Priority_Model - Controller form the given Model
   * @returns {Array}  Array of Messages from respective Priority Collection
   */

  getPriority_Messages (Priority_Model) {
    return new Promise((resolve, reject) => {
      Priority_Model.get({query: [{$limit: this.throttle_rate}]}).then(messages_array => {
        if (!null && messages_array.length > 0) {
          _.forEach(messages_array, (message) => {
            this.setChucked_Messages(message);
            Priority_Model.delete(message._id);
          });
          console.log('Something :: Resolving: '.blue, messages_array.length);
          resolve(messages_array.length);
        }else {
          // console.log('Queue Empty :: Resolving: 0')
          resolve(0);
        }
      }).catch(error => {
        console.log('Priority Get Error: ', error);
        logger.error(`Priority Get Error: ${JSON.stringify(error)}`);
        reject(error);
      });
    });
  }
}

module.exports = Throttler;
