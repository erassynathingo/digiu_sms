/**
* @author Erastus Nathingo <contact@erassy.com>
* @module Number_Sorting_Helper
* @description Helper library for sorting mohile number into MNO Arrays
* @param none
* @returns {Array}
*/
let Promise = require('bluebird');
const moment = require('moment');
let _config = require('../config');
let helpers = require('../libraries/api.helpers');
const { MongoClient } = require('mongodb');
const config = require('../config');
const Agenda = require('agenda');
const Log = require('../libraries/logger.lib');
let logger = new Log();

let MSISDNS = {
  MTC: [],
  TNMobile: [],
  invalidMSISDN: []
};

let Sorter = function () {

  /**
 * Assign the project to an employee.
 * @param {Object} employee - The employee who is responsible for the project.
 * @param {string} employee.name - The name of the employee.
 * @param {string} employee.department - The employee's department.
 */
  this.sort = (MSISDN_array) => {
    console.log('Sorting MSISDNS..', MSISDN_array);
    let newMSISDN = null;
    this.trash();
    for (let i = 0; i < MSISDN_array.length; i++) {
      newNum = MSISDN_array[i].toString();

      let reverseStr = (newNum) => {
        return newNum.split('').reverse().join();
      };
      newMSISDN = reverseStr(newNum);
      this.populate(newMSISDN);
    }
    return Promise.resolve(MSISDNS);
  };

  this.populate = (data) => new Promise((resolve, reject) => {
    switch (true) {
      case data.charAt(16) == 8 && data.charAt(14) == 1: {
        MSISDNS.MTC.push(newNum);
        break;
      }
      case data.charAt(16) == 8 && data.charAt(14) == 5: {
        MSISDNS.TNMobile.push(newNum);
        break;
      }
      default: {
        MSISDNS.invalidMSISDN.push(newNum);
      }
    }
    return this;
  });

  this.trash = () => {
    MSISDNS.MTC = [];
    MSISDNS.TNMobile = [];
    MSISDNS.invalidMSISDN = [];
    return this;
  };

  this.prepare = (MNO, object, msisdns) => {
    // console.log("\n\nPreparing : MNO: ", MNO, "OBJECT: ", object, "MSISDNS: ",msisdns)
    return new Promise((resolve , reject) => {
      let prepared = object;
      prepared.recepients = msisdns;
      prepared.mno = MNO;
      prepared.message_id = MNO + '_' + moment().format('YYYYMMDDhhmmss');
      resolve(prepared);
      //prepared.recepients = [];
    });
  };

  /**
  * @description Schedules messages to be sent at specified time and date
  * @name setAvailable
  * @function
  * @param {Boolean} status - Availability of the Send function
  * @return undefined
  */
  this.schedule = (P0, P1, P2, P3, object) => {
    console.log('Scheduling.. ');
    const agenda = new Agenda({ db: { address: config.db_url, collection: 'scheduled_messages' } });
    agenda.define(`Send SMS`, { priority: 'high' }, (job, done) => {
      helpers.duplicate(object).then(duplicated => {
        return helpers.prioritize(P0, P1, P2, P3, duplicated);
      });
      if (done) {
        job.remove();
      }
    });

    agenda.on('ready', function (job) {
      console.log(`${JSON.stringify(object)} Job has been scheduled`);
      logger.log(`${JSON.stringify(object)} Job has been scheduled`);
      agenda.schedule(object.scheduling.dateTime, 'Send SMS', object);
      agenda.start();
    }).on('start', () => {
      console.log(`${JSON.stringify(object)} Job is starting`);
      logger.log(`${JSON.stringify(object)} Job is starting`);
    }).on('complete', () => {
      console.log(`${JSON.stringify(object)} Job is completed`);
      logger.log(`${JSON.stringify(object)} Job is completed`);
    }).on('complete', (job) => {
      logger.log(`Job Completed:\t ${JSON.stringify(job)}`);
      console.log(`Job Completed:\t ${JSON.stringify(job)}`);
    }).on('fail:Send SMS', (err, job) => {
      logger.error(`Job failed with error: %s' ${err.message} Job:\t ${JSON.stringify(job)}`).log(`Job failed with error: %s' ${err.message} Job:\t ${JSON.stringify(job)}`);
      console.log('Job failed with error: %s', err.message, '\tJob: ', job);
    });
  };
};

module.exports = Sorter;
