
/**
 * Connect to the mongodb using mongoose library
 * Exports the mongoose connection to the database
 * @module Database_config
 * @author Erastus Nathingo <contact@erassy.com>
 * @description authorization route
 * @type Module config|Module config
 * @param {String} [config.db_url]
 * @returns Mongoose connection
 * @throws {MongoError} An error.
 * @license MIT
 */
let config = require('../config');
let Promise = require('bluebird');
let mongoose = require('mongoose');
const Log = require('./logger.lib');
const mongoErrorHandler = require('./mongo-db-error-handler');
let logger = new Log();
const MongoDBError = require('./errors/MongoDBError');
const colors = require('colors');



// initialize
mongoose.Promise = Promise;
mongoose.connect(config.db_url, {
    autoReconnect: true,
    useMongoClient: true,
    socketTimeoutMS: 0,
    connectTimeoutMS: 0,
    keepAlive: true,
    reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
    reconnectInterval: 500,
    bufferMaxEntries: 0
}).catch(error => {
    logger.error('Mongoose connection failed...').log('Mongoose connection failed...'); 
    throw new MongoDBError(error.message);
});

// database connection check
mongoose.connection.on('disconnected', () => {
    global.connected = false;
    logger.log('Mongoose failed to connect, retry in progress'); /** @todo Remove */
    throw new MongoDBError('Mongoose failed to connect , retry in progress');
}).on('connecting',()=>{
    logger.log("Establishing connection to MongoDB");
    console.log('Establishing connection to MongoDB'.warn);
}).on('reconnected', () => {
    global.connected = true;
    logger.log('Database reconnected...'); /** @todo Remove */
    console.log('Database reconnected')
}).on('connected', () => {
    global.connected = true;
    logger.log(`Database connected`); /** @todo Remove */
    console.log(`Database connected`.green);
}).on('error', error => {
    logger.error('Database error due to: ', error.message); /** @todo Remove */
    throw new MongoDBError(error.message)
    
});
process.on('SIGINT', function() {
    setTimeout(function(){
    logger.log("Cleaning up.....")
    mongoose.connection.close(()=> { 
        logger.log('Mongoose default connection disconnected through app termination'); 
      console.log('Mongoose default connection disconnected through app termination'.cyan); 
      process.exit(0); 
    }); 
    }, 3000);
  });

module.exports = mongoose;