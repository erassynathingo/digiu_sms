function MongoDBError(message) {
    this.name = 'MongoDBError';
    this.message = message || 'Database Error';
    //this.stack = (new Error()).stack;
    this.status = 900;
}
MongoDBError.prototype = Object.create(Error.prototype);
MongoDBError.prototype.constructor = MongoDBError;

module.exports = MongoDBError;