function SMPPError(message) {
    this.name = 'smppError';
    this.message = message.desc || 'SMPP connection Error';
    this.status_code = message.command_status_name ? message.command_status_name : "Err";
    this.code = message.code ? message.code : 12;
    this.mno = message.mno ? message.mno : "NO MNO SPECIFIED";
    this.stack = (new Error()).stack;
}
SMPPError.prototype = Object.create(Error.prototype);
SMPPError.prototype.constructor = SMPPError;

module.exports = SMPPError;