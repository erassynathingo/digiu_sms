let query_sm_status = [
  {
    'status_code': 0,
    //'message': 'The message is scheduled. Delivery has not yet been initiated',
    //'status': 'SCHEDULED', /**@todo  SK TN Mobile for proper response  SCHEDULED == DELIVERED?*/
    'message': "Delivered to Handset".toUpperCase(),
    'status': 'DELIVERED'
  },
  {
    'status_code': 1,
    'message': 'The message is enroute.',
    'status': 'ENROUTE'
  },
  {
    'status_code': 2,
    'message': "Delivered to Handset".toUpperCase(),
    'status': 'DELIVERED'
  },
  {
    'status_code': 3,
    'message': 'The SMSC was unable to deliver the message in a specified amount of time.For instance when the phone was turned off'.toUpperCase(),
    'status': 'EXPIRED'
  },
  {
    'status_code': 4,
    'message': 'The message was deleted.',
    'status': 'DELETED'
  },
  {
    'status_code': 5,
    'message': "The Network Operator was unable to deliver the message".toUpperCase(),
    'status': 'UNDELIVERABLE'
  },
  {
    'status_code': 6,
    'message': "Delivered to Network Operator".toUpperCase(),
    'status': 'ACCEPTED'
  },
  {
    'status_code': 7,
    'message': 'Unknown error occured.'.toUpperCase(),
    'status': 'UNKNOWN'
  },
  {
    'status_code': 8,
    'message': "The message was rejected by Network Operator".toUpperCase(),
    'status': 'REJECTED'
  },
  {
    'status_code': 9,
    'message': 'The message was skipped'.toUpperCase(),
    'status': 'SKIPPED'
  }
];

smpp_status = {
    query_sm_status: query_sm_status,
    general_smpp_status: "Something unexpected happened, please contact admininstrator."
}

module.exports = smpp_status;
