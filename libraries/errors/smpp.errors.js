
const status = [
    {
        "status_code": "1",
        "hex_code": "0x1",
        "error_code": "esme_rinvmsglen",
        "message": "short_message field or message_payload TLV has an invalid length (usually too long for the given MC or underlying network technology)"
    },
    {
        "status_code": "2",
        "hex_code": "0x2",
        "error_code": "esme_rinvcmdlen",
        "message": "Command Length is invalid. PDU length is considered invalid, either because the value is too short or too large for the given PDU."
    },
    {
        "status_code": "3",
        "hex_code": "0x3",
        "error_code": "esme_rinvcmdid",
        "message": "Invalid Command ID. Command ID is not recognised, either because the operation is not supported or unknown."
    },
    {
        "status_code": "4",
        "hex_code": "0x4",
        "error_code": "esme_rinvbndsts",
        "message": "Incorrect BIND Status for given command. PDU has been sent in the wrong session state. E.g. sending a submit_sm without first establishing a Bound_TX session state."
    },
    {
        "status_code": "5",
        "hex_code": "0x5",
        "error_code": "esme_ralybnd",
        "message": "A bind request has been issued within a session that is already bound"
    },
    {
        "status_code": "6",
        "hex_code": "0x6",
        "error_code": "esme_rinvprtflg",
        "message": "Invalid Priority Flag. Priority flag contains an illegal or unsupported value."
    },
    {
        "status_code": "7",
        "hex_code": "0x7",
        "error_code": "esme_rinvregdlvflg",
        "message": "Invalid Registered Delivery Flag. Registered field contains an invalid setting."
    },
    {
        "status_code": "8",
        "hex_code": "0x8",
        "error_code": "esme_rsyserr",
        "message": "MC system error indicating that all or part of the MC is currently unavailable. This can be returned in any response PDU"
    },
    {
        "status_code": "10",
        "hex_code": "0xa",
        "error_code": "esme_rinvsrcadr",
        "message": "Invalid Source Address. Source address of message is considered invalid. Usually this is because the field is either too long or contains invalid characters."
    },
    {
        "status_code": "11",
        "hex_code": "0xb",
        "error_code": "esme_rinvdstadr",
        "message": "Invalid Destination Address. Destination address of message is considered invalid. Usually this is because the field is either zero length, too long or contains invalid characters."
    },
    {
        "status_code": "12",
        "hex_code": "0xc",
        "error_code": "esme_rinvmsgid",
        "message": "Message ID is invalid. Message ID specified in cancel_sm, query_sm or other operations is invalid."
    },
    {
        "status_code": "13",
        "hex_code": "0xd",
        "error_code": "esme_rbindfail",
        "message": "A generic failure scenario for a bind attempt. This may be due to a provisioning error, incorrect password or other reason. A MC will typically return this error for an invalid system_id, system_type, password or other attribute that may cause a bind failure"
    },
    {
        "status_code": "14",
        "hex_code": "0xe",
        "error_code": "esme_rinvpaswd",
        "message": "Invalid Password. Password field in bind PDU is invalid. This is usually returned when the length is too short or too long. It is not supposed to be returned when the ESME has specified the incorrect password."
    },
    {
        "status_code": "15",
        "hex_code": "0xf",
        "error_code": "esme_rinvsysid",
        "message": "Invalid System ID. The System ID field in bind PDU is invalid. This is usually returned when the length is too short or too long. It is not supposed to be returned when the ESME has specified the incorrect system id."
    },
    {
        "status_code": "17",
        "hex_code": "0x11",
        "error_code": "esme_rcancelfail",
        "message": "Cancel SM Failed. Generic failure error for cancel_sm operation."
    },
    {
        "status_code": "19",
        "hex_code": "0x13",
        "error_code": "esme_rreplacefail",
        "message": "Replace SM Failed. Generic failure for replace_sm operation."
    },
    {
        "status_code": "20",
        "hex_code": "0x14",
        "error_code": "esme_rmsgqful",
        "message": "Message Queue Full. Used to indicate a resource error within the MC. This may be interpreted as the maximum number of messages addressed to a single destination or a global maximum of undelivered messages within the MC."
    },
    {
        "status_code": "22",
        "hex_code": "0x16",
        "error_code": "esme_rinvsertyp",
        "message": "Invalid Service Type. Service type is rejected either because it is not recognised by the MC or because its length is not within the defined range."
    },
    {
        "status_code": "51",
        "hex_code": "0x33",
        "error_code": "esme_rinvnumdests",
        "message": "Invalid number of destinations. The number_of_dests field in the submit_multi PDU is invalid."
    },
    {
        "status_code": "52",
        "hex_code": "0x34",
        "error_code": "esme_rinvdlname",
        "message": "Invalid Distribution List name. The dl_name field specified in the submit_multi PDU is either invalid, or non-existent."
    },
    {
        "status_code": "64",
        "hex_code": "0x40",
        "error_code": "esme_rinvdestflag",
        "message": "Destination flag is invalid (submit_multi). The dest_flag field in the submit_multi PDU has been encoded with an invalid setting."
    },
    {
        "status_code": "66",
        "hex_code": "0x42",
        "error_code": "esme_rinvsubrep",
        "message": "Submit w/replace functionality has been requested where it is either unsupported or inappropriate for the particular MC. This can typically occur with submit_multi where the context of ‚Äúreplace if present‚Äù is often a best effort operation and MCs may not support the feature in submit_multi. Another reason for returning this error would be where the feature has been denied to an ESME."
    },
    {
        "status_code": "67",
        "hex_code": "0x43",
        "error_code": "esme_rinvesmclass",
        "message": "Invalid esm_class field data. The esm_class field has an unsupported setting."
    },
    {
        "status_code": "68",
        "hex_code": "0x44",
        "error_code": "esme_rcntsubdl",
        "message": "Cannot Submit to Distribution List. Distribution lists are not supported, are denied or unavailable."
    },
    {
        "status_code": "69",
        "hex_code": "0x45",
        "error_code": "esme_rsubmitfail",
        "message": "submit_sm, data_sm or submit_multi failed. Generic failure error for submission operations."
    },
    {
        "status_code": "72",
        "hex_code": "0x48",
        "error_code": "esme_rinvsrcton",
        "message": "Invalid Source address TON. The source TON of the message is either invalid or unsupported."
    },
    {
        "status_code": "73",
        "hex_code": "0x49",
        "error_code": "esme_rinvsrcnpi",
        "message": "Invalid Source address NPI. The source NPI of the message is either invalid or unsupported."
    },
    {
        "status_code": "74",
        "hex_code": "0x4a",
        "error_code": "esme_rinvdstton",
        "message": "Invalid Destination address TON. The destination TON of the message is either invalid or unsupported."
    },
    {
        "status_code": "75",
        "hex_code": "0x4b",
        "error_code": "esme_rinvdstnpi",
        "message": "Invalid Destination address NPI. The destination NPI of the message is either invalid or unsupported."
    },
    {
        "status_code": "77",
        "hex_code": "0x4d",
        "error_code": "esme_rinvsystyp",
        "message": "Invalid system_type field. The System type of bind PDU has an incorrect length or contains illegal characters."
    },
    {
        "status_code": "78",
        "hex_code": "0x4e",
        "error_code": "esme_rinvrepflag",
        "message": "Invalid replace_if_present flag. The replace_if_present flag has been encoded with an invalid or unsupported setting."
    },
    {
        "status_code": "85",
        "hex_code": "0x55",
        "error_code": "esme_rinvnummsgs",
        "message": "invalid number of messages specified for query_last_msgsprimitive"
    },
    {
        "status_code": "88",
        "hex_code": "0x58",
        "error_code": "esme_rthrottled",
        "message": "Throttling error (ESME has exceeded allowed message limits). This type of error is usually returned where an ESME has exceeded a predefined messaging rate restriction applied by the operator."
    },
    {
        "status_code": "98",
        "hex_code": "0x62",
        "error_code": "esme_rinvexpiry",
        "message": "Invalid message validity period (Expiry time). Expiry time is either the incorrect length or is invalid."
    },
    {
        "status_code": "103",
        "hex_code": "0x67",
        "error_code": "esme_rqueryfail",
        "message": "Quota violation, add credit to account"
    },
    {
        "status_code": "194",
        "hex_code": "0xc2",
        "error_code": "esme_rinvparlen",
        "message": "invalid optional parameter length"
    },
    {
        "status_code": "195",
        "hex_code": "0xc3",
        "error_code": "esme_rmissingoptparam",
        "message": "Missing optional parameter"
    },
    {
        "status_code": "196",
        "hex_code": "0xc4",
        "error_code": "esme_rinvoptparamval",
        "message": "invalid optional parameter value"
    },
    {
        "status_code": "254",
        "hex_code": "0xfe",
        "error_code": "esme_rdeliveryfailure",
        "message": "Transaction Delivery Failure. A data_sm or submit_sm operation issued in transaction mode has resulted in a failed delivery."
    },
    {
        "status_code": "255",
        "hex_code": "0xff",
        "error_code": "esme_runknownerr",
        "message": "Unknown Error.Some unexpected error has occurred."
    },
    {
        "status_code": "0",
        "hex_code": "0x0",
        "error_code": "esme_rok",
        "message": "No Error. Specified in a response PDU to indicate the success of the corresponding request PDU. (Ok - Message Acceptable)"
    }
]

module.exports = status