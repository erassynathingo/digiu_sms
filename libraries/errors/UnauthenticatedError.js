function TypeError(message) {
    this.name = 'UnauthenticatedError';
    this.message = message || 'User not Authenticated';
    this.stack = (new Error()).stack;
}
TypeError.prototype = Object.create(Error.prototype);
TypeError.prototype.constructor = TypeError;

module.exports = TypeError;