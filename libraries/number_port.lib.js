/**
 * number_port.lib
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018 DIGIU
 * All rights reserved
 */

const Controller = require('../core/controller.factory');
const Models = require('../core/models.exporter');
const controller = new Models(Controller);

module.exports = {

  /**
   * @description checks if MSISDN is ported
   * @name ported
   * @param {String} msisdn number to be checked for portability
   * @param {String} mno Network Operator to which the number belongs
   * @returns {Boolean}  returns true if MSISDN is ported and false if not
   */
  ported: function (msisdn, mno) {
    switch (mno) {
      case 'MTC':
        return this.checkPotability(controller.mtc_port_list(), msisdn);
        break;
      case 'TN_MOBILE':
        return this.checkPotability(controller.tn_port_list(), msisdn);
        break;
      default:
        break;
    }
  },

  checkPotability: function (ctrl, msisdn) {
    return ctrl.fetch().then(list => {
      list[0].msisdns.forEach(element => {
        if (element.msisdn === msisdn) {
          return Promise.resolve(element);
        }
      });
      return Promise.reject(false);
    });
  }
};
