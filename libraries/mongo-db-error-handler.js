/**
 * MongoDB Error Handler
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018 DIGIU
 * All rights reserved
 */
require('../config/http-status');
const codes = require('../config/status-codes');

/**
 * Handlers errors raised by mongo db
 * @param {object} error - MongoError object.
 */
const mongoDBErrorHandler = (error) => {
  status = 500;
  feedback = {};
  switch (error.code) {
    case 11000:
    console.log(error)
      status = _CONFLICT;
      feedback = codes.duplicate;
      break;
    case 66:
      status = _CONFLICT;
      feedback = {
          message: error.message,
          status: codes.updateError
      }
    default:
      console.log("Default Mongo Error: ",error);
      feedback = {
        message: error.message,
        status: codes.updateError
    }
  }

  return [status, feedback];
};
module.exports = {
  resolve: function (error) {
    return mongoDBErrorHandler(error);
  }
};
