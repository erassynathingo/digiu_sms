/**
 * API Default Error Handler
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @description authorization route
 * All rights reserved
 */
require('../config/http-status');

let _ = require('lodash');
const Logger = require('../libraries/logger.lib');
let logger = new Logger();
const mongoErrorHandler = require('./mongo-db-error-handler');
const errorMessages = require('../config/error-messages.config');

/**
 * Express error handler middleware for handling api errors based on the error raised by the application.
 * The API custom errors include a code which identifies the exact type of error raised.
 * Wrap the error in json, send it back and terminates the request.
 * @param {object} err - Error object
 * @param {object} req - Express request object.
 * @param {object} res - Express response object.
 * @param {function} next - Express next function.
 */
let errorHandler = {
  resolve: (res, error, status) => {
    let feedback = {};
    if (res.headerSent) {
      return next(error);
    }
    switch (error.name) {
      case 'ValidatorError':
      case 'ValidationError':
        feedback.code = error.code || 2;
        status = _UNPROCESSABLE_ENTITY;
        feedback.message = error.message ? error.message : errorMessages.validation;
        break;
      case 'MongoError':
        [status, feedback] = mongoErrorHandler.resolve(error);
        break;
      case 'ForbiddenError':
        feedback.code = error.code || 3;
        status = _FORBIDDEN;
        feedback.message = error.message ? error.message : errorMessages.forbidden;
        break;
      case 'smppError':
        feedback.code = error.code || 3;
        status = _CONFLICT;
        feedback.message = error.message ? error.message : errorMessages.forbidden;
        break;
      case 'UnauthenticatedError':
        feedback.code = error.code;
        status = _UNAUTHORIZED;
        feedback.message = error.message;
        break;

      case 'UnAuthorizedError':
        feedback.code = error.code;
        status = 401;
        feedback.message = (!_.isUndefined(error.message) && !_.isEmpty(error.message)) ? error.message : 'Unauthorized Type Error';
        break;

      case 'MailError':
        response.status = 400;
        response.message = (!_.isUndefined(error.message) && !_.isEmpty(error.message)) ? error.message : 'Email Error';
        break;

      case 'CampainError':
        feedback.code = error.code;
        status = 409;
        feedback.message = (!_.isUndefined(error.message) && !_.isEmpty(error.message)) ? error.message : 'Campaign Error';
        break;

      case 'TypeError':
        feedback.status = (!_.isUndefined(error.status) && !_.isEmpty(error.status)) ? error.status : status;
        feedback.message = (!_.isUndefined(error.message) && !_.isEmpty(error.message)) ? error.message : 'Validation Type Error';
        break;

      case 'CastError':
        feedback.status = (!_.isUndefined(error.status) && !_.isEmpty(error.status)) ? error.status : status;
        feedback.message = error.message ? error.message : errorMessages.castError;
        break;
      case 'isEmptyError':
        feedback.status = (!_.isUndefined(error.status) && !_.isEmpty(error.status)) ? error.status : 411;
        feedback.message = (!_.isUndefined(error.message) && !_.isEmpty(error.message)) ? error.message : 'Item is Empty';
        break;
      case 'NotFoundError':
        feedback.status = 404;
        feedback.message = (!_.isUndefined(error.message) && !_.isEmpty(error.message)) ? error.message : 'Item not Found';
        break;

      default:
        feedback.code = 0;
        status = _INTERNAL_SERVER_ERROR;
        feedback.message = errorMessages.serverError;
        break;
    }
    error.stack = error.stack ? error.stack : 'unknown';
    logger.error(`Name: ${error.name}\nMessage: ${error.message}\nStack: ${error.stack}`);
    console.log(`\n\nName: ${error.name}\nMessage: ${error.message}\nStack: ${error.stack}`);
    feedback.name = error.name || 'Unknown';
    res.status(status).json(feedback).end();
  }
};

module.exports = errorHandler;
