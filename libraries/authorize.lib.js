/**
 * Authorization Handler
 * @module Authorizarion
 * @author Erastus Nathingo <contact@erassy.com>
 * @description authorization handler
 * @type Module config|Module config
 * @param req
 * @returns {true | false}
 * @throws {new UnAuthorizedError()}
 */
let Promise = require('bluebird');
let UnAuthorizedError = require('../libraries/errors/UnAuthorizedError');
const Log = require('../libraries/logger.lib');
const logger = new Log();

class RBAC {
  constructor (roles) {
    this.init(roles);
  }
  init (roles) {
    if (typeof roles !== 'object') {
      Promise.reject(new TypeError('Expected an object as input'));
    }

    this.roles = roles;
    let map = {};
    Object.keys(roles).forEach(role => {
      map[role] = {
        can: {}
      };
      if (roles[role].inherits) {
        map[role].inherits = roles[role].inherits;
      }

      roles[role].can.forEach(operation => {
        if (typeof operation === 'string') {
          map[role].can[operation] = 1;
        } else if (typeof operation.name === 'string'
          && typeof operation.when === 'function') {
          map[role].can[operation.name] = operation.when;
        }
      // Ignore definitions we don't understand
      });
    });

    this.roles = map;
  }

  can (user, operation, params) {
    logger.log(`Authorising ${operation} action for User: ${user._id}, Description: ${params}`);
    return new Promise((resolve, reject) => {
      // Check if role exists
      if (!this.roles[user.role]) {
        reject(new UnAuthorizedError('Authorization failed, User role does not exist'));
      }
      let $role = this.roles[user.role];

      // Check if this role has access
      if ($role.can[operation]) {
        if (typeof $role.can[operation] !== 'function') {
          resolve(true);
        }
        // If the function check passes return true
        if ($role.can[operation](params)) {
          resolve(true);
        }
      }
      // Check if there are any parents
      if (!$role.inherits || $role.inherits.length < 1) {
        reject(new UnAuthorizedError());
      }

      // Check child roles until one returns true or all return false
      resolve($role.inherits.some(childRole => this.can(childRole, operation)));
    });
  }
}

module.exports = RBAC;
