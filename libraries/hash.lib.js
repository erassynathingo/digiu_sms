/**
* @author Erastus Nathingo <contact@erassy.com>
* @module Password_Hasher 
* @description 
* @param
* @returns 
* @throws 
*/

let Promise = require('bluebird')
let bcrypt = Promise.promisifyAll(require('bcryptjs'))
const Log = require('../libraries/logger.lib');
let logger = new Log()
const crypto = require("crypto");
const config = require('../config');

module.exports = function () {
  saltRounds = 13;

  this.encrypt = plain => {
    return new Promise((resolve, reject) => {
      bcrypt.hash(plain, saltRounds).then((hash) => {
        resolve(hash)
      }).catch((error) => {
        console.log('Hashing failed: ', error)
        reject(error)
      })
    })
  }

  this.compare = (plain, password) => bcrypt.compare(plain, password)

  this.encryption = async text => new Promise((resolve, reject) => {
    try {
      let cipher = crypto.createCipher(config.encryptionConfigs.algorithm, config.encryptionConfigs.pass);
      let crypted = cipher.update(text,'utf8','hex')
      crypted += cipher.final('hex');
      resolve(crypted);
    }catch(error){
      reject(error)
    }
  })

  this.decrypt = async text => new Promise((resolve, reject) => {
    try {
      let decipher = crypto.createDecipher(config.encryptionConfigs.algorithm, config.encryptionConfigs.pass)
      let dec = decipher.update(text,'hex','utf8')
      dec += decipher.final('utf8');
      resolve(dec);
    }catch(error){
      reject(error)
    }
  })
}
