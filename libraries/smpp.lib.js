/**
 *  SMPP library functions
 *
 * @author Erastus Nathingo <contact@erassy.com>
 *
 * @copyright (c) 2018 DIGIU
 * All rights reserved
 */
const _ = require('lodash');
const __ = require('underscore');
const Promise = require('bluebird');
const smpp = Promise.promisifyAll(require('smpp'));
const errorHandler = require('./errorHandler.lib');
const SMPPError = require('../libraries/errors/SMPPError');
const Log = require('./logger.lib');
const logger = new Log();
const colors = require('colors');

/** SMPP statuses */

const smpp_status = require('./errors/smpp.errors');

const Controller = require('../core/controller.factory');
const Models = require('../core/models.exporter');
const model = new Models(Controller);
const Interval = require('Interval');

const converter = require('hex2dec');

class SMPP {

  /**
   * Represents a SMPP Instance.
   * @constructor
   * @param {Object} mno_configs - Configurations file for the MNO consuming the SMPP Library Class
   */

  constructor(mno_configs, res) {
    this.mno_configs = mno_configs;
    this.res = res;
    this.session = {};
    let reconnectCount = 0;
  }

  createSession() {
    console.log(`Attempting to connect to: ${this.mno_configs.hostname}:${this.mno_configs.port}`);
    logger.log(`Attempting to connect to: ${this.mno_configs.hostname}:${this.mno_configs.port}`);
    const session = smpp.connect(`smpp://${this.mno_configs.hostname}:${this.mno_configs.port}`);
    this.session = session;
    return this.startEventListener(session);
  }

  startEventListener(session) {
    return new Promise((resolve, reject) => {
      session.on('connect', () => {
        console.log(`\n\nMNO Connected\nHostname:Port => ${this.mno_configs.hostname}:${this.mno_configs.port}`.green);
        logger.log(`\n\nMNO Connected\nHostname:Port => ${this.mno_configs.hostname}:${this.mno_configs.port}`);
        session.bind_transceiver({
          system_id: this.mno_configs.credentials.bind_ID,
          password: this.mno_configs.credentials.password,
          interface_version: 52,
          addr_ton: 1,
          addr_npi: 1,
          system_type: 'SMPP'
        }, (pdu) => {
          resolve(this.responseHandler(pdu, session));
        });
      }).on('close', (error) => {
        logger.error(`\n\nSession Closed: Hostname:Port => ${this.mno_configs.hostname}:${this.mno_configs.port}\n`.red);
        console.error(`\n\nSession Closed: Hostname:Port => ${this.mno_configs.hostname}:${this.mno_configs.port}\n`.red);
      }).on('error', (error) => {
        let reconnectCounts = 0;
        switch (error.code) {
          case 'ECONNREFUSED':
            reject(new SMPPError({
              desc: `SMPP Connection to ${error.address}:${error.port} refused`,
              command_status_name: error.code,
              mno: `${this.mno_configs.hostname}:${this.mno_configs.port}`
            }));
            break;
          case 'ETIMEDOUT':
            reject(new SMPPError(`SMPP Connection to ${error.address}:${error.port} Timed out`, error.code));
            break;
          default:
            reject(new SMPPError(`Unkown SMPP Error`, 'UNKNOWN'));
            break;
        }
      }).on('disconnect', error => {
        logger.error(`\n\nSession Disconnected: Hostname:Port => ${this.mno_configs.hostname}:${this.mno_configs.port}\nUnbinding...\n`);
        console.error(`\n\nSession Disconnected: Hostname:Port => ${this.mno_configs.hostname}:${this.mno_configs.port}\nUnbinding...\n`.red);
        session.unbind();
      }).on('enquire_link', (pdu) => {
        // logger.log(`Enquire_PDU: ${JSON.stringify(pdu)}`)
        // console.log('Enquire_PDU: ', pdu)
        reject(this.responseHandler(pdu, session));
      }).on('unbind', (pdu) => {
        logger.log(`Session Unbound: ${JSON.stringify(pdu)}`);
        console.log('Session Unbound: ', pdu);
        this.responseHandler(pdu, session);
      }).on('pdu', (pdu) => {
        this.responseHandler(pdu, session).then(data => null).catch(err => null);
      });
    });
  }

  /**
   * @description transforms looks up PDU status key {pdu command status}
   * @name responseHandler
   * @param {Object} pdu - PDU response from MC events fired from the SMPP session
   * @returns {Promise}  Promise object representing the transformed pdu response
   */

  async responseHandler(pdu, session) {
    const status = smpp_status.find(element => element.error_code.toUpperCase() == this.lookupPDUStatusKey(pdu.command_status));

      if (status.error_code.toUpperCase() == 'ESME_ROK') {
        switch (pdu.command) {
          case 'unbind_resp':
            logger.log(`Session Unbound: ${JSON.stringify(pdu)}`);
            console.log('Session Unbound: ', pdu);
            break;

          case 'enquire_link_resp':
            logger.log(`Enquiring enquire_link_resp to: ${this.mno_configs.hostname}`);
            break;

          case 'bind_transceiver_resp':
            logger.log(`Successfully binded with ${this.mno_configs.hostname}:${this.mno_configs.port} as a Transceiver`);
            console.log(`Successfully binded with ${this.mno_configs.hostname}:${this.mno_configs.port} as a Transceiver`.green);
            return Promise.resolve({
              session: session
            });
            break;

          case 'enquire_link':
            session.enquire_link_resp();
            // logger.log(`Responding enquire_link_resp to: ${this.mno_configs.hostname}`)
            break;

          case 'deliver_sm':
            try{
              logger.log(`Incoming SMS FROM: ${pdu.source_addr.toString()} TO: ${pdu.destination_addr} TEXT: ${pdu.short_message.message}`);
              console.log(`Incoming SMS FROM: ${pdu.source_addr.toString()} TO: ${pdu.destination_addr} TEXT: ${pdu.short_message.message}`.cyan);

              /** Delivery Status logic Here */
              if (pdu.esm_class === 4) {
                let arr = pdu.short_message.message.split(' ');
                const message_id =  converter.decToHex(arr[0].replace('id:', '')).substring(2).toUpperCase().trim();

                let finalStatus = { sms_status: this.setStatus(arr[7].replace('stat:', '')), recepient: pdu.destination_addr, type: '' };

                const OT =  await model.sms().findInArray({OTPS: {$elemMatch: {message_id:message_id}}});

                const AL =  await model.sms().findInArray({ALERTS: {$elemMatch: {message_id:message_id}}});

                console.log(`\nALERTS Length: ${AL.length}`.gray);
                console.log(`\nOTP Length: ${OT.length}`.gray);

                if (AL.length > 0) {
                  finalStatus.type = 'ALERTS';
                  const alertJob = await model.sms().updateQueryStatus(message_id, finalStatus);
                } if (OT.length > 0) {
                  finalStatus.type = 'OTP';
                  const otpJob = await model.sms().updateQueryStatus(message_id, finalStatus);
                }
                session.deliver_sm_resp({ sequence_number: pdu.sequence_number });
              }
              /** Campaign Logic in the else */
              else {
                try {

                  const camps = await model.campaign().getActiveCampaign();
                  let text = {
                    message: pdu.short_message.message,
                    source_addr: pdu.source_addr,
                    dest_addr: pdu.destination_addr
                  }

                  const results = async function () {
                    let key = {
                      switcher: "default",
                      campID: null
                    };
                    for (let index = 0; index < camps.length; index++) {

                      if (text.message.toUpperCase().includes(camps[index].campaignId)) {
                        key.switcher = "campaign";
                        key.campID = camps[index]._id;
                        return Promise.resolve(key);
                        break;
                      }
                    }
                    return Promise.resolve(key);
                  }
                  const switchKey = await results();
                  switch (switchKey.switcher) {
                    case 'campaign':
                      const updateCampJob = await model.campaign().updateCampaign(switchKey.campID, text)
                      break;
                    case 'default':
                      const updateIbocJob = await model.inbox().create({
                        body: {
                          smsc: `Hostname: ${this.mno_configs.hostname},  Port: ${this.mno_configs.port},  Full Name: ${this.mno_configs.full_Name}`,
                          body: text
                        }
                      })
                      break;
                    default:
                      break;
                  }
                } catch (error) {
                  logger.log(' Campaign unsuccessful').error(error.message);
                }

            }
            session.deliver_sm_resp({ sequence_number: pdu.sequence_number });
          }
          catch(error){
            logger.error(`Error: ${JSON.stringify(error)}`);
          }

          session.deliver_sm_resp({ sequence_number: pdu.sequence_number });
          break;
          default:
            break;
        }
      } else {
        logger.error(`SMPP ERROR for : ${this.mno_configs.hostname}:${this.mno_configs.port} \n\n ${JSON.stringify(status)}`.red);
        return Promise.reject(new SMPPError({
          desc: status.message,
          command_status_name: status.error_code,
          mno: `${this.mno_configs.hostname}:${this.mno_configs.port}`
        }));
      }
  }

  /**
   * @description queries the MC and checks of the communication path between an ESME and a MC
   * @name isBound
   * @param {Object} session - session object of the bounded SMSC
   * @returns {Boolean}  returns true when bound and false otherwise
   */

  isBound(session) {
    session.enquire_link();
  }


  /**
   * @description sets the delivery status
   * @name setStatus
   * @param {String} status - Message delivery status from the SMSC
   * @returns {Object}  returns human readable form of the status
   */

  setStatus(status) {
    let state = '';
    switch (status) {
      case 'ACCEPTD':
        state = {
          message: "Delivered to Network Operator".toUpperCase(),
          status: "ACCEPTED".toUpperCase()
        }
        break;
      case 'DELIVRD':

        state = {
          message: state = "Delivered to Handset.".toUpperCase(),
          status: "DELIVERED".toUpperCase()
        }

        break;

      case 'REJECTD':
        state = {
          message: state = "The message was rejected by Network Operator".toUpperCase(),
          status: "REJECTED".toUpperCase()
        }
        break;

      case 'UNDELIV':
        state = {
          message: state = "The Network Operator was unable to deliver the message".toUpperCase(),
          status: "UNDELIVERABLE".toUpperCase()
        }
        break;
      default:
        state = {
          message: state = "SMS Status is UNKNOWN".toUpperCase(),
          status: "UNKNOWN".toUpperCase()
        }
        break;
    }

    return state;
  }

  /**
   * @description matches a status from array and returns it
   * @name findStatus
   * @param {string} status - status to find
   * @returns {Object}  full object of found status
   */

  reconnect(reconnectCounts = 0, error) {
    if (error.code == 'ECONNREFUSED') {
      Interval.run(() => {
        reconnectCounts++;
        smpp.connect(`smpp://${this.mno_configs.hostname}:${this.mno_configs.port}`);
        console.log(`Reconnect attempt NO:${reconnectCounts} to => ${this.mno_configs.hostname}:${this.mno_configs.port}`.green);
        logger.log(`Reconnect attempt NO:${reconnectCounts} to => ${this.mno_configs.hostname}:${this.mno_configs.port}`.green);
      }, 10000).until(() => reconnectCounts == 5).end(() => {
        console.log(`Concluded Reconnect attempts to => ${this.mno_configs.hostname}:${this.mno_configs.port} \nPlease contact ${this.mno_configs.full_Name}\nPerforming Unbind`.magenta);
        logger.log(`Concluded Reconnect attempts to => ${this.mno_configs.hostname}:${this.mno_configs.port} \nPlease contact ${this.mno_configs.full_Name}\nPerforming Unbind`.magenta);
        console.log('Session: ', session);
        session.unbind();
        reject(new SMPPError({
          desc: `SMPP Connection to ${error.address}:${error.port} refused`,
          command_status_name: error.code
        }));
      });
    }
  }

  /**
   * @description queries the MC and checks of the status of the SMS based on the message ID
   * @name checkMessageStatus
   * @param {String} message_id - message ID of the message being queried
   * @param {Object} configs - represents the SMSC configs of that message
   * @param {Object} session - represents the SMSC configs of that message
   * @returns {Object<PDU>}  PDU response of the message status from the SMSC
   */

  checkMessageStatus(message_id) {
    this.session.query_sm({
      message_id: message_id,
      source_addr_ton: this.mno_configs.config.source_addr_ton,
      source_addr_npi: this.mno_configs.config.source_addr_npi,
      source_addr: this.mno_configs.config.source_addr
    }, pdu => {
      console.log(`\nCheking Message ${message_id}\n Status:  ${pdu} '\n`);
      this.responseHandler(pdu, this.session);
    });
  }

  connectSMPP() {
    console.log('Attempting to reconnect to ' + settings);
    session.connect();
    return this;
  }

  lookupPDUStatusKey(pduCommandStatus) {
    for (var k in smpp.errors) {
      if (smpp.errors[k] == pduCommandStatus) {
        return k;
      }
    }
    return this;
  }
}

module.exports = SMPP;
