/**
 * ESB Intergrator
 * @module ESB Intergrator
 * @author Erastus Nathingo <contact@erassy.com>
 * @description handles all integration with ESB
 */

const sql = require('mssql');
const mysql = require('promise-mysql');
const oracledb = require('oracledb');


class ESB_Intergrator {

    /**
     * Represents a ESB_Intergrator Instance.
     * @constructor
     * @param {Object} Controllers - Controllers for Mongoose Models  {Mongo DB Models}
     * @param {Object} session - session of the current MNO for binding purposes
     * @param {Object} mno_configs - Configurations file for the MNO consuming the Message State Controller
     */

    constructor(configs) {
        this.connection = null;
        this.options = configs;
    }

    /**
     * @name oracle
     * @description connects current instance of mssql to ESB
     * @returns connection instances
     * @param {Object} options
     */
   

    async oracle(query) {
        const opts = this.options;
        return new Promise(async function (resolve, reject) {
            let conn;
            console.log(`\n\nOPTIONS:  ${opts.server}:${opts.port}/${opts.service}`);
            console.log('\n\nQuery: ', query);
            

            try {
                conn = await oracledb.getConnection({
                    user: opts.user,
                    password: opts.password,
                    connectString: `${opts.server}:${opts.port}/${opts.service}`
                });

                let result = await conn.execute(query);
                resolve(result.rows);

            } catch (err) { // catches errors in getConnection and the query
                reject(err);
            } finally {
                if (conn) { // the conn assignment worked, must release
                    try {
                        await conn.release();
                    } catch (e) {
                        console.log('Err: ',e);
                        console.error(e);
                    }
                }
            }
        });
    }

    async sql (query) {
        const opts = this.options;
        try {
            await sql.connect({
                user: opts.user,
                password: opts.password,
                server: opts.server, // You can use 'localhost\\instance' to connect to named instance
                database: opts.database,
             
                options: {
                    encrypt: false // Use this if you're on Windows Azure
                }
            })
            const result = await sql.query`select * from sms_localization FOR JSON PATH`
            console.log('RESULT: ', result);
        } catch (err) {
           console.log('Error: ', err);
        }
    }

    async executeQuery(query) {
        try {
            let res = await this.sql(query);
            console.log(res);
            return res;
        } catch (err) {
            console.error(err);
            return err;
        }
    }
}

module.exports = ESB_Intergrator;