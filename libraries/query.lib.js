/**
 * query.lib.js
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018
 * All rights reserved
 */

// module dependencies
const _ = require('lodash');
const queryString = require('mongo-querystring');
const helpers = require('./api.helpers');
const iterators = require('./iterators.utilities');
/**
 * Build the aggregation pipeline for mongodb based on the query of the request.
 * @see {@link https://www.npmjs.com/package/mongo-querystring }
 */

 class QueryBuilder  {

    constructor(controller){
        this.dictionary = controller.dictionary;

    }

/**
 * Wrapper to translate data from database keys to api keys.
 * @param {object} - The data coming from the database to be translated.
 * @return {Promise}
 */
  translate = (data) => {
    console.log('Translating...'); /** @todo Remove */
    return helpers.mapInverse(data, this.dictionary);
  };

/**
 * Wrapper to reverse data keys from api keys to database keys.
 * @param {object} data - The data coming from the api.
 * @param {array} exclude - The keys to be excluded from translation and from propagating to database.
 * @return {Promise}
 */
  reverse = (data, exclude = []) => {
    console.log('Reversing...'); /** @todo Remove */
    return helpers.map(_.omit(data, exclude), this.dictionary);
  };
 }