/**
 * Attach methods and properties to Express response and request objects
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @description authorization route
 * All rights reserved
 */
const errors = require('../config/error-handler');
const errorDefs = require('../libraries/errors/errors');
const session = require('../core/session');
const codes = require('../config/status-codes');
const status = require('../config/http-status');
const responder = require('../middleware/responder.middleware');
const errMessages = require('../config/error-messages.config');

/**
 * Attach objects and functions to request objects;
 * @param {object} req - Express request object.
 * @param {object} res - Express response object.
 * @param {function} next - Express next function
 */
function attach(req, res, next) {
    // attach to request object
    try {
        req.$session = session(req);

        //attach to response object
        res.$codes = codes;
        res.$status = status;
        res.$json = (feed = {}, status = 200, emptyStatus = 201) => {
            responder.json(req, res, feed, status, emptyStatus);
        };
        res.$errMsg = errMessages;
        res.$error = errors;
        res.$errors = errorDefs;
        next();
    } catch (error) {
        next(error);
    }
}

module.exports = attach;