/**
 * @author Erastus Nathingo <contact@erassy.com>
 * @module Logger_Library
 * @description Logs to the console && file logs
 * @param {String}
 * @returns log()
 * @throws 
 */

const _config = require('../config')
let bunyan = require('bunyan')
let fs = require('fs')

let logPath = './logs/'+_config.api_title+'.error.log'
let info_logPath = './logs/'+_config.api_title+'.trace.log'
let fatalLogPath = './logs/'+_config.api_title+'.fatal_logs.log';


let logger = bunyan.createLogger(
  {
    name: _config.api_title,
    streams: [
      {
        level: 'trace',
        name: 'Trace_Log',
        path: info_logPath, // log info and above to a file
        type: 'rotating-file',
        period: '1d', // daily rotation
        count: 2, // keep 2 back copies2
        name: 'Trace Logs'
      },
      {
        level: 'error',
        path: logPath, // log ERROR and above to a file
        type: 'rotating-file',
        period: '1d', // daily rotation
        count: 2, // keep 2 back copies
        name: 'Errors'
      },
      {
        level: 'fatal',
        path: fatalLogPath, // log ERROR and above to a file
        type: 'rotating-file',
        period: '1d', // daily rotation
        count: 2, // keep 2 back copies
        name: 'Fatal_Errors'
      }
    ]
  })

module.exports = function () {
  this.log = item => {
    logger.trace(item)
    return this;
  }
  this.trace = item => {
    logger.trace(item)
    return this;
  }
  this.error = text => {
    logger.error(text)
    return this;
  }

  this.debug = text => {
    logger.debug(text)
    return this;
  }

  this.warn = text => {
    logger.warn(text)
    return this;
  }

  this.info = text => {
    logger.trace(item)
    return this;
  }
}