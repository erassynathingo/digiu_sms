/**
 * mail.lib.js
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018 DIGIU
 * All rights reserved
 */
const express = require('express');
const Promise = require('bluebird');
const nodemailer = Promise.promisifyAll(require('nodemailer'));
const _config = require('../config');
let MailError = require('../libraries/errors/MailError');
const Email = require('email-templates');

const node_mailer = function(configs) {

  this.transporter = () => nodemailer.createTransport(configs);

  /**
   * @description Sends Email based on given options/ params
   * @name sendMail
   * @param {Object} options.to recepient of the email
   * @param {Object} options.from sender of the email
   * @param {Object} options.html contains an html body template of the email
   * @param {Object} options.subject contains the subject of the email
   * @returns {Promise<Object>}  Either resolves or reject response sent from email host
   * @see https://nodemailer.com/usage/
   */
  this.sendMail = (options) => this.transporter().sendMail(options)


  this.send = (options) => {
    const email = new Email({
      message: {
        from: options.from
      },
      // uncomment below to send emails in development/test env:
       send: true,
      transport: this.transporter()
    });
     
    email.send({
        template: 'mars',
        message: {
          to: options.to
        },
        locals: options.locals
      })
      .then(console.log)
      .catch(new MailError);
  }
};

module.exports = node_mailer;
