/**
 * @author Erastus Nathingo <contact@erassy.com>
 * @module Message_State_Ctrl
 * @description COntrols the Message States in the Db
 */
const Controller = require('../core/controller.factory');
const Models = require('../core/models.exporter');
const models = new Models(Controller);
const Log = new require('../libraries/logger.lib');
const logger = new Log();
const moment = require('moment');
const smpp_status = require('../libraries/errors/smpp.status');
const Interval = require('Interval');
const _ = require('lodash');
let __ = require('underscore');

module.exports = class Message_State_Ctrl {

    /**
     * Represents a Throttler Instance.
     * @constructor
     * @param {Object} Controllers - Controllers for Mongoose Models  {Mongo DB Models}
     * @param {Object} session - session of the current MNO for binding purposes
     * @param {Object} mno_configs - Configurations file for the MNO consuming the Message State Controller
     */

    constructor (Controllers, session, mno_configs) {
      this.State_Controllers = Controllers;
      this.session = session;
      this.mno_configs = mno_configs;
      this.matchRules = {
        queryOTP: { 'valid_until': {$gte: new Date()}},
        quertALERT: { 'expiry_date': {$gte: new Date()}},
        sweep: { 'expiry_date': {$lte: new Date()}}
      };
    }

    async init () {
      logger.log(`Initiating Message State-Checker functions for ${this.mno_configs.full_Name}`);
      console.log(`Initiating Message State-Checker functions for ${this.mno_configs.full_Name}`);
      Interval.run(() => {
        try{
          /** initiates the functions for querying */
          this.starter();
          /** initiates the sweeper functions for cleaning the enroute DB */
          this.sweep();
        }catch(error){
          logger.error(`Querying functions error: ${JSON.stringify(error)}`);
        }
      }, 5000);
    }

    async starter(){
      const validSmses =  await this.getValidSMSes();
      /** loop for querying each message in the enroute queue */
      _.forEach(validSmses,(message) =>{
        this.queryRules(message);
      })
    }

    /**
     * @description Gets all Valid enroute Messages
     * @name getValidEnrouteMessages
     * @returns {Promise}  returns a Promise of Valid enroute messages object
     */

    deleteMessage (message_id) {
      return this.State_Controllers.primary_enroute.delete(message_id);
    }

    /**
     * @description Gets all Valid enroute Messages
     * @name getValidEnrouteMessages
     * @returns {Promise}  returns a Promise of Valid enroute messages object
     */

    updateMessage (message_id, body) {
      return models.sms().updateQueryStatus(message_id, body)
    }

    /**
     * @description builds then inserts enroute Message from normal OTP or ALERTS sms
     * @name setEnrouteMessages
     * @param sms SMS object to be inserted in to the primary enroute collection
     * @returns {Promise}  returns a Promise of enroute messages object
     */
    setEnrouteMessages (sms) {
      if (sms.type === 'OTP' || sms.type === 'ALERTS') {
        let enroutedSMS = {
          _id: sms.response.message_id,
          message_state: sms.sms_status.status,
          queries: [],
          message_details: sms
        };
        return this.State_Controllers.primary_enroute.createSMS(enroutedSMS);
      }
      return Promise.resolve('Invalid Type');
    }





    /**
     * @description Gets all SMS's where 'expiry_date' is more than current time 
     * @name getValidSMSes
     * @returns {Array}  returns a array of NOT EXPIRED SMSes
     */

    getValidSMSes () {
      //console.log("\n\nCollecting for Querying\n".green)
      return this.State_Controllers.primary_enroute.get({ query: [{$match: this.matchRules.quertALERT}]});
    }

    /**
     * @description Compares two date values
     * @name compareDates
     * @param {Date} date_1 date to be compared
     * @param {Date} date_2 date to be compared
     * @returns {Boolean} true if the dates are equal, false if otherwise
     */
    async queryRules (message) {
      try {
            if(message.message_details.type === 'OTP'){
              /** Query only of OTP is valid} */
              if(new Date(message.valid_until) > new Date()){

                /** check the message status */
                let messageStatus = await this.checkMessageStatus(message._id);

                /** append the status to the message object */
                let single = __.extend(message.message_details, {sms_status: messageStatus});
                /** update the message in the DB */
                let updateMessage = await this.updateMessage(message._id, single);

                if(messageStatus.status === 'PENDING' || messageStatus.status === 'SCHEDULED'){
                  //console.log("OTP Not in final State__DO NOT DELETE".red);
                }else{
                  /** delete  the message from the enroute primaries DB */
                  let deleteMessage =  this.deleteMessage(message._id);
                }
              }
            }
            if(message.message_details.type === 'ALERTS'){
                /** check the message status */
                let messageStatus = await this.checkMessageStatus(message._id);
                /** append the status to the message object */
                let single = __.extend(message.message_details, {sms_status: messageStatus});
                /** update the message in the DB */
                let updateMessage = await this.updateMessage(message._id, single);

                if(messageStatus.status === 'PENDING' || messageStatus.status === 'SCHEDULED'){
                  //console.log("ALERT Not final State__DO NOT DELETE".red);
                }else{
                  /** delete  the message from the enroute primaries DB */
                  let deleteMessage =  this.deleteMessage(message._id);
                }
    
            }

      }catch(error){
        logger.error(`Querying Rules Error: ${JSON.stringify(error)}`);
      }
    }


    /**
     * @description queries the MC for the status of the SMS based on the message_id
     * @name checkMessageStatus
     * @param {String} message_id - message ID of the message being queried
     * @returns {Promise<Object>}  PDU response of the message status from the SMSC
     */

    checkMessageStatus (message_id) {
      logger.log(`\nCheking Message: ${message_id}`);
      return new Promise((resolve, reject) => {
        this.session.query_sm({
          message_id: message_id,
          source_addr_ton: this.mno_configs.config.source_addr_ton,
          source_addr_npi: this.mno_configs.config.source_addr_npi,
          source_addr: this.mno_configs.config.source_addr
        }, pdu => {
          resolve(this.lookupMessageStateKey(pdu.message_state));
        });
      });
    }

    



    /**
     * @description call the broom function every 3 days to clean out expired messages
     * @name sweep
     * @returns {null}
     */

    sweep (){
      Interval.run(() => {
        this.broom();
      }, 259200000);
    }

    



    /**
     * @description deletes expires enroute smses from database
     * @name broom
     * @returns {null}
     */

    async broom (){
      try{
        let expired = await this.getExpiredMessages();
        expired.forEach((message)=>{
          /** delete  the message from the enroute primaries DB */
          this.deleteMessage(message._id);
        })
      }catch(error){
        logger.error(`Enroute message Sweeper Error: ${JSON.stringify(error)}`);
      }
    }




    /**
     * @description gets all expires enroute smses from database
     * @name getExpiredMessages
     * @returns {Array<Object>} returns an array of all the expired messages objects
     */

    getExpiredMessages (){
      return this.State_Controllers.primary_enroute.get({ query: [{$match: this.matchRules.sweep}]});
    }

    /**
     * @description looks up the actual alphanumeric status key from the given message state parameter
     * @name lookupMessageStateKey
     * @param {Number} message_state - command status of the returned PDU
     * @returns {Object<PDU>}  PDU response of the message status from the SMSC
     */
    lookupMessageStateKey (message_state) {
      for (let i = 0; i < 10; i++) {
        if (smpp_status.query_sm_status[i].status_code == message_state) {
          return smpp_status.query_sm_status[i];
        }
      }
    }
    
};

