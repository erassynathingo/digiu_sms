/**
 * @author Erastus Nathingo <contact@erassy.com>
 * @description Entry Point for DIGIU_SMS_API
 */

let express = require('express'),
  http = require('http'),
  config = require('./config'),
  compression = require('compression'),
  morgan = require('morgan'),
  headers = require('./middleware/headers'),
  helmet = require('helmet'),
  bodyParser = require('body-parser'),
  errorhandler = require('errorhandler'),
  errorNotify = require('./middleware/error.middleware'),
  responseTime = require('response-time'),
  cors = require('cors'),
  attach = require('./libraries/attach.lib'),
  corsOpt = require('./middleware/cors.options'),
  init = require('./core/init');

// =================   ROUTES DEFINITIONS ================ //

auth = require('./routes/authentication.route'),
users = require('./routes/users.route'),
departments = require('./routes/departments.route'),
sms = require('./routes/sms.route'),
campaigns = require('./routes/campaign.route');
mno = require('./routes/mno.route'),
scheduled = require('./routes/scheduled.route'),
agenda = require('./routes/agenda.route'),
roles = require('./routes/roles.route'),
number_porting = require('./routes/number.porting.route');

let app = express(),
  port = process.env.PORT || config.port,
  server = http.createServer(app).listen(port, () => {
    console.log(`Running ${config.api_title} on ${config.hostname}:${config.port}`);
     init.startThrottler();
  });

config.env === 'development' ? app.use(errorhandler({
  log: errorNotify.notification,
  dumpExceptions: true,
  showStack: true
})) : null;

app.set('Title', `${config.api_title}`)
  .use(morgan('dev'))
  .use(headers)
  .use(bodyParser.urlencoded({ extended: true, limit: '1000mb' }))
  .use(bodyParser.json({ limit: '100mb' }))
  .use(bodyParser.raw({ limit: '100mb' }))
  .use(responseTime())
  .use(attach)
  /* Security*/
  .use(helmet())
  .use(cors(corsOpt.getCORSoptions()))
  /* Speed & Efficiency */
  .use(compression())
  // ================  ROUTERS ====================>>//
  .use('/auth', auth)
  .use('/users', users)
  .use('/departments', departments)
  .use('/sms', sms)
  .use('/mno', mno)
  .use('/campaigns', campaigns)
  .use('/scheduled', scheduled)
  .use('/dash', agenda)
  .use('/roles', roles)
  .use('/number_porting', number_porting)
  // ================  Default Route Handler ====================>>//
  .use('/', (req, res)=>res.status(200).json({
    API: `${config.api_title} SMS API`,
    Copyright: `@${config.api_title} 2018`,
    Version: `1.0.0`
  }))

