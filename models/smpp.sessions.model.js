/**
* @author Erastus Nathingo <contact@erassy.com>
* @module SMPP_Sessions
* @description Provides a mongoose model for SMPP Sessions with MNO's
* @param
* @returns Session
* @throws 
*/
let _config = require('../config');
const Model = require('../core/model');
const moment = require('moment')

/** Session schema definitions */
let sessionSchema =
{
  _id: { type: String, required: true},
  SMSC_config: { type: Object, required: true},
  SMSC_ID: { type: String, required: true },
  command: { type: String, required: true},
  entry_date: { type: Date, default: moment().toDate()}
};

let dictionary = {
  id: '_id',
  SMSC_config: 'SMSC_config',
  SMSC_ID: 'SMSC_ID',
  command: 'command',
  entry_date: 'entry_date'
};

const createExclude = ['entry_date'];
const updateExclude = ['_id', 'entry_date', '_id', 'SMSC_config','SMSC_ID' ];
const readExclude = [];
const model = Model.create('SMPP_Sessions', sessionSchema, dictionary, createExclude, updateExclude);
module.exports = model.getModel();
