/**
* @author Erastus Nathingo <contact@erassy.com>
* 
* @module OTP_Model
* @description Provides a mongoose model for OTP's, Alerts and single message
* @param
* @returns User
* @throws 
*/
let _config = require('../config');
const Model = require('../core/model');
const moment = require('moment');

/** OTP schema definitions */

let otpSchema =
{
  author: { type: Object, required: true},
  originator: { type: String, required: true},
  body: { type: String, required: true },
  recepient: { type: Array, required: true},
  type: { type: String, required: true },
  gateway: { type: String, required: true, default: 'DIGIU Hub'},
  priority : { type: Number, default: 3 },
  message_id:  { type: String},
  response: {type: Object, default: {}},
  sms_status: {type: Object, default: {}},
  mno: { type: String, required: true},
  entry_date: { type: Date, default: new Date()}
};

let dictionary = {
  id : '_id',
  author: 'author',
  originator: 'originator',
  body: 'body',
  recepient: 'recepient',
  type: 'type',
  message_id: 'message_id',
  gateway: 'gateway',
  priority: 'priority',
  sms_status: 'sms_status',
  mno: 'mno',
  entry_date: 'entry_date',
  response: 'response'
};

const createExclude = ['entry_date'];
const updateExclude = ['_id', 'entry_date','author'];
const readExclude = [];
const model = Model.create('OTP', otpSchema, dictionary, updateExclude);
module.exports = model.getModel()
