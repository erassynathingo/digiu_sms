/**
* @author Erastus Nathingo <contact@erassy.com>
* @module SMS_Model
* @description Provides a mongoose model for SMS's
* @param
* @returns User
* @throws 
*/
let users = require('../models/users.model');
let _config = require('../config');
const Model = require('../core/model');
const moment = require('moment');

/** SMS schema definition */
let smsSchema = {
  _id: { type: String},
  OTPS: {type: Array, default: [], uppercase: true},
  BULK: {type: Array, default: [], uppercase: true},
  ALERTS: {type: Array, default: [], uppercase: true}
};

let dictionary = {
  _id: '_id',
  OTPS: 'OTPS',
  BULK: 'BULK',
  ALERTS: 'ALERTS',
};

const createExclude = [];
const updateExclude = ['_id' ];
const readExclude = [];
const model = Model.create('Messages', smsSchema, dictionary, createExclude, updateExclude);
module.exports = model.getModel();
