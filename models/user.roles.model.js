
/**
* @author Erastus Nathingo <contact@erassy.com>
* @module User_Roles_Model
* @description Provides a mongoose model for User Roles
* @returns {Model<User_Roles>}
*/
let users = require('../models/users.model');
let _config = require('../config');
const Model = require('../core/model');
const moment = require('moment');

/** User Roles Schema definitions */
let userRolesSchema =
{
  _id: { type: String },
  role: {
    name: {type: String, required: true, unique: true},
    can: { type: Array, required: true, default: []},
    inherits: { type: Array, default: []}
  },
  entry_date: { type: Date, default: moment().toDate() }
};

let dictionary = {
  _id: '_id',
  role: 'role',
  entry_date: 'entry_date'
};

const createExclude = ['entry_date'];
const updateExclude = ['id_number', 'entry_date', 'username'];
const readExclude = [];
const model = Model.create('Users_Roles', userRolesSchema, dictionary, createExclude, updateExclude);
module.exports = model.getModel();
