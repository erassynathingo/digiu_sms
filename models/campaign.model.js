
/**
 * campaign.model.js
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018
 * All rights reserved
 * 
 * @module Campaigns_Model
 * @description Provides a mongoose model for Campaigns
 * @param
 * @returns Campaigns Model
 */

let _config = require('../config');
const Model = require('../core/model');
const moment = require('moment');

let campaignSchema = {
  _id: { type: String, default: 'camp'+ '_' + moment().format('YYYYMMDDhhmmss')},
  campaignName: {type: String, required: true },
  campaignId: {type: String, required: true },
  status: {type: String, default: "INACTIVE", uppercase: true},
  replies: {type: Array, default: []},
  authorId: {type: String, required: true },
  departmentId: {type: String, required: true },
  message: {type: String},
  entry_date: { type: Date, default: moment().toDate()}
}; 

let dictionary = {
  _id: '_id',
  campaignName: 'campaignName',
  campaignId: 'campaignId',
  status: 'status',
  replies: 'replies',
  authorId: 'authorId',
  departmentId: 'departmentId',
  message: 'message',
  entry_date: 'entry_date'
};


const createExclude = ['entry_date'];
const updateExclude = ['_id', 'entry_date', 'campaignId', 'replies', 'campaignName'];
const readExclude = [];
const model = Model.create('Campaigns', campaignSchema, dictionary, createExclude, updateExclude);
module.exports = model.getModel();