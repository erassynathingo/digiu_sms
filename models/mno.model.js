/**
* @author Erastus Nathingo <contact@erassy.com>
* @module Clients_Model
* @description Provides a mongoose model for Clients
* @param
* @returns MNO Model
* @throws 
*/
let users = require('../models/users.model');
let _config = require('../config');
const Model = require('../core/model');
const moment = require('moment');
/** Client schema definitions */

let mnoSchema =  {
    _id: {
        type: String
    },
    full_Name: {
        type: String
    },
    short_Name: {
        type: String
    },
    hostname: {
        type: String
    },
    port: {
        type: String
    },
    interface: {
      protocol: {
          type: String
      },
      version: {
          type: Number
      },
    },
    credentials: {
      bind_ID: {
          type: String
      },
      password: {
          type: String
      },
    },
    config:{
      source_addr: {
          type: String
      },
      source_addr_ton: {
          type: Number
      },
      dest_addr_ton: {
          type: Number
      },
      esme_addr_ton: {
          type: Number
      },
      addr_ton: {
          type: Number
      },
      addr_npi: {
          type: Number
      },
      system_type: {
          type: String
      },
      source_addr_npi: {
          type: Number
      },
      esme_addr_npi: {
          type: Number
      },
      dest_addr_npi: {
          type: Number
      },
    },
    throttle_rate: {
        type: Number
    },
    
    country : {
        name: String,
        code: String
    },
    entry_date: {
        type: Date,
        default: moment().toDate()
    }
  }
  

let dictionary = {
    _id: "_id",
    full_Name: "full_Name",
    hostname: "hostname",
    short_Name: "short_Name",
    short_Code: "short_Code",
    bind_ID: "bind_ID",
    hostname: "hostname",
    port: "port",
    interface: "interface",
    credentials: "credentials",
    config: "config",
    throttle_rate: "throttle_rate",
    country:  "country",
    entry_date: "entry_date"
  };


const createExclude = ['entry_date'];
const updateExclude = ['_id', 'entry_date'];
const readExclude = [];
const model = Model.create('Mobile_Network_Operators', mnoSchema, dictionary, createExclude, updateExclude);
module.exports = model.getModel();
