/**
* @author Erastus Nathingo <contact@erassy.com>
* @module SMS_Model
* @description Provides a mongoose model for SMS's
* @param
* @returns User
* @throws 
*/
let users = require('../models/users.model');
let _config = require('../config');
const Model = require('../core/model');
const moment = require('moment');

/** User schema definitions */
let smsc_responseSchema =
{
    smsc: { type: String},
    body: { type: Object},
    entry_date: { type: Date, default: moment().toDate()}
  };

let dictionary = {
    smsc: 'smsc',
    entry_date: 'entry_date',
    body: 'body'
  };

const createExclude = ['entry_date'];
const updateExclude = ['_id', 'entry_date'];
const readExclude = [];
const model = Model.create('smsc_responses', smsc_responseSchema, dictionary, createExclude, updateExclude);
module.exports = model.getModel();


