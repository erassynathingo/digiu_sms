/**
* @author Erastus Nathingo <contact@erassy.com>
* @module Departments_Model
* @description Provides a mongoose model for Departments
* @param
* @returns Departments Model
* @throws 
*/
let users = require('../models/users.model');
let _config = require('../config');
const Model = require('../core/model');
const moment = require('moment');

/** Department schema definitions */

let departmentSchema = {
  _id: {type: String},

  fullName: {type: String, required: true},

  shortName: {type: String, required: true},
  costCenter: {type: String, required: true},
  members: {type: Array, default: []},
  units: {
    value: {type: Number,required: true},
    currency: {type: String, default: 'DIGIU_HUB_COIN'}
  },
  entry_date: { type: Date, default: moment().toDate()},
  campaigns: { type: Array, default: [] }
};

let dictionary = {
  _id: '_id',
  fullName: 'fullName',
  shortName: 'shortName',
  costCenter: 'costCenter',
  members: 'members',
  units: 'units',
  campaigns: 'campaigns',
  entry_date: 'entry_date',
};

const createExclude = ['entry_date'];
const updateExclude = ['_id', 'entry_date'];
const readExclude = [];
const model = Model.create('Departments', departmentSchema, dictionary, createExclude, updateExclude);
module.exports = model.getModel();
