/**
* @author Erastus Nathingo <contact@erassy.com>
* @module SMS_Model
* @description Provides a mongoose model for TN_Mobile Priority 1 SMSes
* @param
* @returns Priority 1 Smses
* @throws 
*/
let _config = require('../../../config');
const Model = require('../../../core/model');
const moment = require('moment');

/** TN_Mobile Priority 1 Smses schema definitions */
let TN_Priority_1_Schema =
{
  message_id: { type: String, default: ""},
  originator: { type: String, default: ""},
  body: { type: String },
  recepient: { type: String},
  type: { type: String},
  gateway: { type: String, default: 'DIGIU Hub'},
  scheduling: { type: Object},
  priority: { type: String},
  mno: { type: String, default: ''},
  author: { type: Object,default: {}},
  response: {type: Object, default: {}},
  sms_status: {type: Object, default: {
    message: "The Network Operator has not yet acknowledged the message".toUpperCase(),
    status: "PENDING".toUpperCase()
}},
  entry_date: { type: Date, default: moment().toDate()}
};

let dictionary = {
  message_id: 'message_id',
  originator: 'originator',
  body: 'body',
  recepient: 'recepient',
  type: 'type',
  gateway: 'gateway',
  scheduling: 'scheduling',
  priority: 'priority',
  mno: 'mno',
  author: 'author',
  response: 'response',
  sms_status: 'sms_status',
  entry_date: 'entry_date',
};

const createExclude = ['entry_date'];
const updateExclude = ['_id', 'entry_date', 'message_id', 'author' ];
const readExclude = [];
const model = Model.create('TN_Priority_1', TN_Priority_1_Schema, dictionary, createExclude, updateExclude);
module.exports = model.getModel();
