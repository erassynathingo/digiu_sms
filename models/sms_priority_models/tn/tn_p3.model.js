/**
* @author Erastus Nathingo <contact@erassy.com>
* @module SMS_Model
* @description Provides a mongoose model for TN_Mobile Priority 3 SMSes
* @param
* @returns Priority 3 Smses
* @throws 
*/
let _config = require('../../../config');
const Model = require('../../../core/model');
const moment = require('moment');

/** TN_Mobile Priority 3 Smses schema definitions */
let TN_Priority_3_Schema =
{
  message_id: { type: String, default: ""},
  originator: { type: String},
  body: { type: String },
  recepient: { type: String},
  type: { type: String},
  gateway: { type: String, default: 'DIGIU Hub'},
  priority: { type: String},
  mno: { type: String},
  author: { type: Object},
  response: {type: Object, default: {}},
  sms_status: {type: Object, default: {
    message: "The Network Operator has not yet acknowledged the message".toUpperCase(),
    status: "PENDING".toUpperCase()
}},
  entry_date: { type: Date, default: new Date()}
};

let dictionary = {
  message_id: 'message_id',
  originator: 'originator',
  body: 'body',
  recepient: 'recepient',
  type: 'type',
  gateway: 'gateway',
  priority: 'priority',
  mno: 'mno',
  author: 'author',
  response: 'response',
  sms_status: 'sms_status',
  entry_date: 'entry_date',
};

const createExclude = ['entry_date'];
const updateExclude = ['_id', 'entry_date', 'message_id', 'author' ];
const readExclude = [];
const model = Model.create('TN_Priority_3', TN_Priority_3_Schema, dictionary, createExclude, updateExclude);
module.exports = model.getModel();
