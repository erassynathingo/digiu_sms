
/**
* @author Erastus Nathingo <contact@erassy.com>
* @module tn_Ported_List
* @description Provides a mongoose model for tn's ported numbers
*/
let _config = require('../../../config');
const Model = require('../../../core/model');
const moment = require('moment');

let tn_port_listSchema =
{
    _id: { type: String, default: 'tn_portlist_01' },
  msisdns: {type: Array},
  last_updated: { type: Date, default: moment().toDate() }
};

let dictionary = {
  _id: '_id',
  msisdns: 'msisdns',
  last_updated: 'last_updated'
};

const createExclude = ['entry_date'];
const updateExclude = ['_id'];
const readExclude = [];
const model = Model.create('tn_port_list', tn_port_listSchema, dictionary, createExclude, updateExclude);
module.exports = model.getModel();
