/**
* @author Erastus Nathingo <contact@erassy.com>
* @module SMS_Model
* @description Provides a mongoose model for MTC Priority 3 SMSes
* @param
* @returns Priority 3 Smses
* @throws 
*/
let _config = require('../../../config');
const Model = require('../../../core/model');
const moment = require('moment');

/** MTC Priority 3 Smses schema definitions */
let MTC_Priority_3_Schema =
{
  originator: { type: String},
  body: { type: String },
  recepient: { type: String},
  type: { type: String},
  gateway: { type: String, default: 'DIGIU Hub'},
  priority: { type: String},
  mno: { type: String},
  author: { type: Object},
  response: {type: Object, default: {}},
  sms_status: {type: Object, default: {
    message: state = "Delivered to Handset".toUpperCase(),
    status: "DELIVERED".toUpperCase()
  }
},
  entry_date: { type: Date, default: new Date()}
};

let dictionary = {
  message_id: 'message_id',
  originator: 'originator',
  body: 'body',
  recepient: 'recepient',
  type: 'type',
  gateway: 'gateway',
  priority: 'priority',
  mno: 'mno',
  author: 'author',
  response: 'response',
  sms_status: 'sms_status',
  entry_date: 'entry_date',
};

const createExclude = ['entry_date'];
const updateExclude = ['_id', 'entry_date', 'message_id', 'author' ];
const readExclude = [];
const model = Model.create('MTC_Priority_3', MTC_Priority_3_Schema, dictionary, createExclude, updateExclude);
module.exports = model.getModel();
