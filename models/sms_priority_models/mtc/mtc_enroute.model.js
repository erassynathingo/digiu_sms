/**
* @author Erastus Nathingo <contact@erassy.com>
* @module enroute.model
* @description Provides a mongoose model for enroute messages
* @param
* @returns {Object<Model>}
* @throws 
*/
let _config = require('../../../config');
const Model = require('../../../core/model');
const moment = require('moment');

/** User schema definitions */
let mtcEnrouteSchema = 
  {
    _id: { type: String },
    message_state: { type: String, required: true },
    expiry_date: { type: Date, default: moment().add(3,'days') },
    queries: { type: Array, default: [] },
    message_details: { type: Object, required: true },
    valid_until: { type: Date, default: moment().add(15,'minutes')},
    entry_date: { type: Date, default: moment().toDate() }
  };

 let dictionary = {
    _id: '_id',
    message_state: 'message_state',
    expiry_date: 'expiry_date',
    queries: 'queries',
    entry_date: 'entry_date',
    valid_until: 'valid_until',
    message_details: 'message_details'
  }

  const createExclude = ["entry_date"];
  const updateExclude = ["_id", "entry_date"];
  const readExclude = [];
  const model = Model.create('mtc_enroute_primary', mtcEnrouteSchema, dictionary, createExclude, updateExclude);
  module.exports = model.getModel();
