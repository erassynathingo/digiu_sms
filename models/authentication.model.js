/**
 * authorization.model.js
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018 DIGIU
 * @license MIT
 */
const Model = require('../core/model');
const moment = require('moment');
const schema = {
    _id: { index: true, unique: true, type: String },
    password: { type: String, required: true },
    entryDate: { type: Date, default: moment().toDate() }
};

const dictionary = {
    staff_id: "_id",
    password: "password",
    entry_date: "entryDate"
};

const createExclude = ["entry_date"];
const updateExclude = ["staff_id", "entry_date"];
const readExclude = [];

const model = Model.create('Authentication', schema, dictionary);
module.exports = model.getModel();