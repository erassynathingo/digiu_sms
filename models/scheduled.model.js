/**
* @author Erastus Nathingo <contact@erassy.com>
* 
* @module OTP_Model
* @description Provides a mongoose model for Scheduled Messages
* @returns Scheduled Messages
* @throws 
*/
let _config = require('../config');
const Model = require('../core/model');
const moment = require('moment');

/** Scheduled Messages schema definitions */


let scheduledSchema ={
    _id: { type: String, default: 'sch'+ '_' + moment().format('YYYYMMDDhhmmss')},
    name: { type: String, required: true},
    data: {type: Object, default: {}},
    type: { type: String, required: true},
    priority: { type: String, required: true},
    nextRunAt: { type: Date},
    lastModifiedBy:  { type: String, required: true},
    entry_date: { type: Date, default: moment().toDate()},
  };

let dictionary = {
    _id: '_id',
  name: 'name',
  data: 'data', 
  type: 'type',
  priority: 'priority',
  nextRunAt: 'nextRunAt',
  lastModifiedBy: 'lastModifiedBy',
  entry_date: 'entry_date',
};

const createExclude = ['entry_date'];
const updateExclude = ['_id', 'entry_date'];
const readExclude = [];
const model = Model.create('scheduled_messages', scheduledSchema, dictionary, updateExclude);
module.exports = model.getModel();
