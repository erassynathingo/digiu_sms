/**
* @author Erastus Nathingo <contact@erassy.com>
* @module Authentication_Middleware
* @description 
* @params 
* @returns 
* @throws 
*/

let express = require('express');
let config = require('../config');
let Promise = require('bluebird');
let jwt = require('jsonwebtoken');
let _ = require('lodash');
let __ = require('underscore');
const Log = require('../libraries/logger.lib');
const Controller = require('../core/controller.factory');
const UserModel = require('../models/users.model').model;
const controller = Controller.create('users.model');
let Hash = Promise.promisifyAll(require('../libraries/hash.lib'));
let UnAuthorizedError = require('../libraries/errors/UnAuthorizedError');
let UnauthenticatedError = require('../libraries/errors/UnauthenticatedError');
const dict = require('../helpers/dictionary');
let map = require('../helpers/map');
let hash = new Hash();
let logger = new Log();
const moment = require('moment');

module.exports = function () {
  this.user = null;
  /* middleware to check is a user is authenticated */
  this.authenticate = (req, res, next) => {
    let token = req.body.token || req.query.token || req.headers['Authorization'] || req.headers['authorization'];
    if (!token) {
      res.$error.resolve(res, new UnauthenticatedError(res.$codes.noToken.message));
    }
    else if (_.isEmpty(token)) {
      res.$error.resolve(res, new UnauthenticatedError(res.$codes.noToken.message));
    }
    else if (token) {
      Promise.try(() => jwt.verify(token, config.JWT.secret)).then(decoded => {
        req.currentUser = decoded;
        Promise.resolve(next());
      }).catch(jwt.JsonWebTokenError, err => {
        res.$error.resolve(res, new UnauthenticatedError(res.$codes.unexpectedToken.message));
      }).catch(jwt.NotBeforeError, err => {
        res.$error.resolve(res, new UnauthenticatedError(res.$codes.unexpectedToken.message));
      }).catch(jwt.TokenExpiredError, err => {
        res.$error.resolve(res, new UnauthenticatedError(res.$codes.expiredToken.message));
      }).catch(err => {
        res.$error.resolve(res, new UnauthenticatedError(res.$codes.unexpectedToken.message));
      });
    }else {
      res.$error.resolve(res, new UnauthenticatedError(res.$codes.unexpectedToken.message));
    }
  };

  /* logout a user and destry session */
  this.logout = (req) => {
    return new Promise((resolve, reject) => {
      req.session.destroy(function (err) {
        err ? reject(err) : resolve(true);
      });
    });
  };

  this.login = function (req) {
    const searchID = req.body.id;
    return UserModel.findOne({ '_id': searchID }).then(user => {
      if (_.isEmpty(user)) {
        return Promise.reject(new UnAuthorizedError('Authentication failed, User does not exist'));
      }
      this.user = {
        _id: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
        username: user.username,
        email: user.email,
        role: user.role,
        cellphone: user.cellphone,
        entry_date: user.entry_date,
        approvedBy: user.approvedBy,
        approved: user.approved,
        department: user.department
      };

      // check if the account is currently locked
      if (user.lockUntil && user.lockUntil > moment().toDate()) {
        logger.log(`User is Account Locked!!\n${JSON.stringify(user)}`);
        // just increment login attempts if account is already locked
        return user.incLoginAttempts().then(data => {
          return Promise.reject(new UnAuthorizedError('User Account Temporalily Locked, Too many failed login attempts'));
        });
      }
      if (user.lockUntil && user.lockUntil < moment().toDate()) {
        logger.log(`Reseting ATTEMPTS FOR ${JSON.stringify(user)}`);
        var updates = {
          $set: { loginAttempts: 0 },
          $unset: { lockUntil: 1 }
        };
        user.update(updates).catch(Promise.resolve);
      }
      return hash.compare(req.body.password, user.password).then(resp => {
        if (!resp) {
          // creds are incorrect, so increment login attempts before responding
          return user.incLoginAttempts().then(data => {
            return Promise.reject(new UnAuthorizedError('Authentication failed, Wrong password'));
          }).catch(error => Promise.reject(error));
        }else if (resp) {
          // if there's no lock or failed attempts, just return the user
          if (!user.lockUntil) {
            req.user = _.omit(this.user, ['password']);
            // reset attempts and lock info
            var updates = {
              $set: { loginAttempts: 0 },
              $unset: { lockUntil: 1 }
            };
            user.update(updates).catch(Promise.resolve);
            return this.sign({
              payload: {
                _id: user._id,
                firstName: user.firstName,
                lastName: user.lastName,
                username: user.username,
                email: user.email,
                role: user.role,
                cellphone: user.cellphone,
                entry_date: user.entry_date,
                approvedBy: user.approvedBy,
                approved: user.approved,
                department: user.department
              },
              secret: config.JWT.secret,
              options: config.JWT.options
            });
          }
        }
      }).catch(Promise.reject);
    }).catch(Promise.reject);
  };

  this.sign = (conf) => jwt.sign(conf.payload, conf.secret, conf.options);
};
