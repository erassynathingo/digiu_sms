/**
 * Feedback handler for the api. If a request comes in with a suppress query parameter set to true,
 * the api will return no content to the client.
 * @author Erastus Nathingo <contact@erassy.com>
 * @module Hub_API_Responser 
 * @param {object} req - Express request object.
 * @param {object} res - Express response object.
 * @param {object} feed - JSON feedback
 * @param {number} status - HTTP status code when there is content.
 * @param {number} emptyStatus - HTTP status when content is to be supressed.
 */


const config = require('../config')
const Log = require('../libraries/logger.lib')
let logger = new Log()

const json = function (req, res, feed = {}, status = 200, emptyStatus = 201) {
    logger.log('JSON Feedback'); /** @todo Remove */
    if (req.method === 'GET' && feed === null) {
        res.status(404).json({
            code: 404,
            message: 'Item(s) you are looking for could not be found.'
        }).end();
    }
    else if (req.query && req.query.supress === 'true') {
        res.status(emptyStatus).end();
    } else {
        res.status(status).json(feed).end();
    }
}

module.exports = {
    json: json
};