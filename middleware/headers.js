/**
* @author Erastus Nathingo <contact@erassy.com>
* @module Request_headers
* @description Defines request header contents
* @params 
* @returns 
* @throws 
*/

module.exports = function (req, res, next) {
  // Cross Origin headers//
  res.header('Access-Control-Allow-Origin', req.get('origin'))
  res.header('Access-Control-Allow-Credentials', true); 
  res.header('Content-Type', 'application/json')
  res.setHeader('Access-Control-Allow-Methods', 'POST,GET,OPTIONS,PUT,DELETE,PATCH')
  res.setHeader('Access-Control-Allow-Headers', 'Authorization, Content-Type, Content-Range, Content-Disposition, Content-Description')
  next()
}