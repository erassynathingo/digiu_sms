/**
 * Import and export of responder
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018 DIGIU
 * All rights reserved
 */
const responder = require('../lib/api.responder');

module.exports = responder;