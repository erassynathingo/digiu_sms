let roles  = {

    Admin: {
        can: ['READ', 'CREATE', 'PATCH', 'DELETE', 'UPDATE', 'PUBLISH', 'QUERY']
    },
    guest: {
        can: ['READ']
    },
    standard : {
        can: ['READ', 'CREATE', 'UPDATE']
    },
    consultant: {
        can: ['PATCH'],
        inherits: ['standard']
    },
    developer: {
        can: [''],
        inherits: ['Admin']
    }
}
module.exports = roles;

/*const patch = 1;    //00000001
const read = 2;     //00000010
const del = 4;      //00000100
const update = 8    //00001000
const create = 16   //00010000
const super = 16    //00100000

const permissions = patch | read | update | create; //00000011 = 3

//

if(permissions & read === read){

}

if(permissions | del | patch === 3)*/

