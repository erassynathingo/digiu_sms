/**
 * error-codes.js
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018 DIGIU
 * All rights reserved
 */
/**
 * Declare and defines the status codes and their friendly messages.
 * They are bundle here together to have a central place and make it easier to document.
 */
const statusCodes = {
  noToken: {
    message: 'User could not be authenticated, no token provided.',
    code: 40
  },
  invalidToken: {
    message: 'User could not be authenticated, invalid token.',
    code: 41
  },
  unexpectedToken: {
    message: 'User could not be authenticated, unexpected token type.',
    code: 42
  },
  noAuthUser: {
    message: 'User could no be authorized, authorization for user does not exist.',
    code: 43
  },
  noPermissions: {
    message: 'User could not be authorized, permissions missing.',
    code: 44
  },
  notAuthorized: {
    message: 'User is not authorized to perform this action.',
    code: 45
  },
  wrongToken: {
    message: 'User could not be authorized, wrong token.',
    code: 46
  },
  expiredToken: {
    message: 'User could not be authorized, Token Expired. Please login again',
    code: 47
  },
  duplicate: {
    message: 'Action could not be completed, due to database duplicate constraints.',
    code: 49
  },
  dbUnknown: {
    message: 'Action could not be completed, database failure.',
    code: 51
  },
  _55: {
    message: 'User could not be authorized, server error',
    code: 55
  },
  _56: {
    message: 'Action reversed, all necessary steps could not complete successfully.',
    code: 56
  },
  nullModify: {
    code: 60,
    message: 'The item you are trying to modify does not exist.'
  },
  updateError: {
    code: 66,
    message: 'The item you are trying to update could not modified'
  },
  dbError: {
    code: 67,
    message: 'The database is not available'
  }
};
module.exports = statusCodes;
