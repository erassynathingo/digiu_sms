/**
 * error-handler.js
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018 DIGIU
 * All rights reserved
 */
const errorHandler = require('../libraries/errorHandler.lib');
module.exports = errorHandler;