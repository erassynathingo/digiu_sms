/**
 * @author Erastus Nathingo <contact@erassy.com>
 * @description Contains the neccessary configuration for running the application
 * @module DIGIU_SMS_configuration_file
 * @returns configs for @name
 * @throws no Errors
 */

const env = process.env.NODE_ENV;
const os = require('os');
const colors = require('colors');
const originsWhitelist = ['http://localhost:3000'];
const responses = {
  status_codes: {
    'CREATED': 200,
    'FETCHED': 200,
    'DELETED': 200,
    'UPDATED': 200,
    'NOT_CREATED': 102,
    'NOT_FETCHED': 202,
    'NOT_DELETED': 302,
    'NOT_UPDATED': 402,
    'BAD_REQUEST': 500,
    'SERVER_ERROR': 600,
    'FORBIDDEN': 700,
    'UNAUTHORIZED': 401,
    'CONFLICT': 409,
    'NOT FOUND': 404
  },
  messages: {
    'created': 'Item(s) creation successful',
    'notCreated': 'Item(s) creation unsuccessful',
    'fetch': 'Item(s) retrieval successful',
    'notFetched': 'Item(s) retrieval unsuccessful',
    'updated': 'Items(s) update successful',
    'notUpdated': 'Item(s) update unsuccessful',
    'deleted': 'Item(s) deletion successful',
    'notDeleted': 'Item(s) deletion unsuccessful',
    'badRequest': 'Invalid request',
    'serverError': 'Requets could be completed due to server issues',
    'forbidden': 'You do not have the permission to perform this action',
    'unAuthorized': 'You are not authorized to perform this action'
  }
};

const dev = {
  port: 9003,
  hostname: os.hostname(),
  api_title: 'DIGIU_SMS',
  db_url: 'mongodb://localhost:27017/DIGIU_SMS?authMechanism=SCRAM-SHA-1&authSource=admin',
  auth: {
    secret: process.env.AUTH_SECRET | 'S0ftwar3_3ng1n33ring_i5_7h3_B357',
    name: 'DIGIU_SMS',
    saveUninitialized: false,
    resave: true,
    maxAge: 3600000

  },
  response: responses,
  originsWhitelist: originsWhitelist,
  ESB_SMS_URL: 'https://exampleESB.digiu.me',
  SMSC_BIND_CONFIGS: {
    MTC_ALPHA: false,
    MTC_NUMERIC: false,
    TN_MOBILE_ALPHA: false,
    TN_MOBILE_NUMERIC: false,
    TEST_SERVER: false
  },
  SMTP_CONFIGS: {
    service: 'smtp.office365.com',
    pool: true,
    secure: false,
    port: 587,
    proxy: '',
    host: 'email@digiu.me',
    auth: {
      user: 'digiu01',
      pass: 'password1'
    },
    logger: false, // log to console
    debug: true,
    tls: {
      ciphers: 'SSLv3',
      rejectUnauthorized: false
    }
  },

  encryptionConfigs: {
    algorithm: 'aes-256-cbc',
    pass: process.env.ENCRYPTION_PASS || 'P@55w0rd@D1G1U_G@t3w@y_P@55',
    key: process.env.ENCRYPTION_KEY || 'P@55w0rd@D1G1U_G@t3w@y_K3y'
  },

  JWT: {
    secret: 'P@55w0rd@D1G1U_G@t3w@y_P@55',
    options: {
      expiresIn: '24h',
      issuer: 'DIGIU SMS GATEWAY'
    }
  }
},
prod = {
  port: 9003,
  hostname: os.hostname(),
  api_title: 'DIGIU_SMS',
  db_url: 'mongodb://localhost:27017/DIGIU_SMS?authMechanism=SCRAM-SHA-1&authSource=admin',
  auth: {
    secret: process.env.AUTH_SECRET | 'S0ftwar3_3ng1n33ring_i5_7h3_B357',
    name: 'DIGIU_SMS',
    saveUninitialized: false,
    resave: true,
    maxAge: 3600000

  },
  response: responses,
  originsWhitelist: originsWhitelist,
  ESB_SMS_URL: 'https://exampleESB.digiu.me',
  SMSC_BIND_CONFIGS: {
    MTC_ALPHA: false,
    MTC_NUMERIC: false,
    TN_MOBILE_ALPHA: false,
    TN_MOBILE_NUMERIC: false,
    TEST_SERVER: false
  },
  SMTP_CONFIGS: {
    service: 'smtp.office365.com',
    pool: true,
    secure: false,
    port: 587,
    proxy: '',
    host: 'email@digiu.me',
    auth: {
      user: 'digiu01',
      pass: 'password1'
    },
    logger: false, // log to console
    debug: true,
    tls: {
      ciphers: 'SSLv3',
      rejectUnauthorized: false
    }
  },

  encryptionConfigs: {
    algorithm: 'aes-256-cbc',
    pass: process.env.ENCRYPTION_PASS || 'P@55w0rd@D1G1U_G@t3w@y_P@55',
    key: process.env.ENCRYPTION_KEY || 'P@55w0rd@D1G1U_G@t3w@y_K3y'
  },

  JWT: {
    secret: 'P@55w0rd@D1G1U_G@t3w@y_P@55',
    options: {
      expiresIn: '24h',
      issuer: 'DIGIU SMS GATEWAY'
    }
  }
},
uat = {
  port: 9003,
  hostname: os.hostname(),
  api_title: 'DIGIU_SMS',
  db_url: 'mongodb://localhost:27017/DIGIU_SMS?authMechanism=SCRAM-SHA-1&authSource=admin',
  auth: {
    secret: process.env.AUTH_SECRET | 'S0ftwar3_3ng1n33ring_i5_7h3_B357',
    name: 'DIGIU_SMS',
    saveUninitialized: false,
    resave: true,
    maxAge: 3600000

  },
  response: responses,
  originsWhitelist: originsWhitelist,
  ESB_SMS_URL: 'https://exampleESB.digiu.me',
  SMSC_BIND_CONFIGS: {
    MTC_ALPHA: false,
    MTC_NUMERIC: false,
    TN_MOBILE_ALPHA: false,
    TN_MOBILE_NUMERIC: false,
    TEST_SERVER: false
  },
  SMTP_CONFIGS: {
    service: 'smtp.office365.com',
    pool: true,
    secure: false,
    port: 587,
    proxy: '',
    host: 'email@digiu.me',
    auth: {
      user: 'digiu01',
      pass: 'password1'
    },
    logger: false, // log to console
    debug: true,
    tls: {
      ciphers: 'SSLv3',
      rejectUnauthorized: false
    }
  },

  encryptionConfigs: {
    algorithm: 'aes-256-cbc',
    pass: process.env.ENCRYPTION_PASS || 'P@55w0rd@D1G1U_G@t3w@y_P@55',
    key: process.env.ENCRYPTION_KEY || 'P@55w0rd@D1G1U_G@t3w@y_K3y'
  },

  JWT: {
    secret: 'P@55w0rd@D1G1U_G@t3w@y_P@55',
    options: {
      expiresIn: '24h',
      issuer: 'DIGIU SMS GATEWAY'
    }
  }
};


console.log(`CONFIG_ENVIRONMENT = ${env}`.cyan);
const config = {
  dev,
  uat,
  prod
};
module.exports = config[env];